angular.module('mainApp').controller('newRecurringCtrl', function($scope, $state, $objectstore, $uploader, $mdDialog, $window, $objectstore, $auth, $timeout, $q, $http, $mdToast, $rootScope, recurringInvoiceService, $filter, $location, UploaderService, MultipleDudtesService) {
      $scope.list = [];
      $scope.TDinvoice = {};
      $scope.total = 0;
      $scope.product = {};
      $scope.TDinvoice.profileName = 'N/A'
      $scope.Showdate = false;
      $scope.TDinvoice.Startdate = new Date();
      $scope.showEditCustomer = false;
      $scope.dueDtaesShow = false;
      //checks whether the use has selected a name or not. if the name is selecter the it enebles the user to edit the customer details
      $scope.selectedItemChange = function(c) {
         $scope.showEditCustomer = true;
      };
      //check whether the the user select the dudate. if the use enters a due date the payment type will be change to custom
      $scope.$watch("TDinvoice.duedate", function() {
         if ($scope.TDinvoice.duedate != null) {
            $scope.TDinvoice.termtype = "Custom";
         }
      });
      $scope.sortableOptions = {
         containment: '#sortable-container'
      };
      //pops a dialog box to edit or view product
      $scope.viewProduct = function(obj) {
            $mdDialog.show({
               templateUrl: 'Invoicepartials/showproduct.html',
               controller: 'productCtrl',
               locals: {
                  item: obj
               }
            });
         }
         //pops a dialog box which enble the user to upload the files
      $scope.upload = function(ev) {
            $mdDialog.show({
               templateUrl: 'Invoicepartials/showUploader.html',
               targetEvent: ev,
               controller: 'UploadCtrl',
               locals: {
                  dating: ev
               }
            })
         }
         //pops a dialog box which enble the user to change currency
      $scope.acceptAccount = function(ev, user) {
            $mdDialog.show({
               templateUrl: 'Invoicepartials/changeCurrency.html',
               targetEvent: ev,
               controller: 'AppCtrl'
            })
         }
         //pops a dialog box which enble the user to add Multiple du dates
      $scope.MultiDuDates = function(data) {
            $mdDialog.show({
               templateUrl: 'Invoicepartials/MultipleDuedates.html',
               controller: function addMultipleDueDates($scope, $mdDialog) {
                  $scope.testarr = [{
                     duedate: '',
                     percentage: '',
                     duDatePrice: '',
                  }];
                  $scope.dateArray = {
                     value: [{
                        duedate: '',
                        percentage: '',
                        duDatePrice: '',
                     }]
                  };
                  $scope.AddDueDates = function() {
                     for (var i = $scope.testarr.length - 1; i >= 0; i--) {
                        MultipleDudtesService.setDateArray({
                           DueDate: $scope.testarr[i].duedate,
                           Percentage: $scope.testarr[i].percentage,
                           dueDateprice: $scope.testarr[i].duDatePrice
                        });
                     };
                     $mdDialog.hide();
                  }
                  $scope.addItem = function() {
                     $scope.testarr.push({
                        duedate: '',
                        percentage: '',
                        duDatePrice: '',
                     });
                  };
                  $scope.removeItem = function(index) {
                     $scope.testarr[i].splice(index, 1);
                     // MultipleDudtesService.removeDateArray(name);
                  };
                  $scope.cancel = function() {
                     $mdDialog.cancel();
                  }
               }
            })
         }
         //Delete added products
      $scope.deleteproduct = function(name) {
            recurringInvoiceService.removeArray(name);
         }
         //dialog box pop up to add product
      $scope.addproduct = function(ev) {
         $mdDialog.show({
            templateUrl: 'Invoicepartials/addproduct.html',
            targetEvent: ev,
            controller: function addProductController($scope, $mdDialog) {
               //add product to the invoice
               $scope.addproductToarray = function() {
                     recurringInvoiceService.setArray({
                        Productname: $rootScope.selectedItemm.valuep.Productname,
                        price: $rootScope.selectedItemm.valuep.costprice,
                        quantity: $scope.tdIinvoice.qty,
                        ProductUnit: $rootScope.selectedItemm.valuep.ProductUnit,
                        discount: $scope.tdIinvoice.MaxDiscount,
                        tax: $rootScope.selectedItemm.valuep.producttax,
                        olp: $scope.tdIinvoice.olp,
                        amount: $scope.Amount
                     });
                     $mdDialog.hide();
                  }
                  //close dialog box
               $scope.cancel = function() {
                     $mdDialog.cancel();
                  }
                  //Uses auto complete to get the product details 
               $rootScope.proload = loadpro();
               $rootScope.selectedItemm = null;
               $rootScope.searchTextt = null;
               $rootScope.querySearchh = querySearchh;

               function querySearchh(query) {
                  $scope.enter = function(keyEvent) {
                     if (keyEvent.which === 13) {
                        if ($rootScope.selectedItemm === null) {
                           $rootScope.selectedItemm = query;
                        } else {}
                     }
                  }
                  $rootScope.results = [];
                  for (i = 0, len = $rootScope.proName.length; i < len; ++i) {
                     if ($rootScope.proName[i].dis.indexOf(query) != -1) {
                        $rootScope.results.push($rootScope.proName[i]);
                     }
                  }
                  return $rootScope.results;
               }
               $rootScope.proName = [];

               function loadpro() {
                     var client = $objectstore.getClient("12thproduct");
                     client.onGetMany(function(data) {
                        if (data) {
                           // $scope.contact =data;
                           for (i = 0, len = data.length; i < len; ++i) {
                              $rootScope.proName.push({
                                 dis: data[i].Productname.toLowerCase(),
                                 valuep: data[i]
                              });
                           }
                        }
                     });
                     client.onError(function(data) {});
                     client.getByFiltering("*");
                  }
                  //calculate the invoice amount for each product
               $scope.calAMount = function() {
                  $scope.Amount = 0;
                  $scope.Amount = ((($rootScope.selectedItemm.valuep.costprice * $scope.tdIinvoice.qty) - (($rootScope.selectedItemm.valuep.costprice * $scope.tdIinvoice.qty) * $scope.tdIinvoice.MaxDiscount / 100)) + (($rootScope.selectedItemm.valuep.costprice * $scope.tdIinvoice.qty)) * $rootScope.selectedItemm.valuep.producttax / 100);
                  return $scope.Amount;
               }
            }
         })
      }
      $scope.contacts = [];
      //dialog box pop up to add customer through invoice
      $scope.addCustomer = function() {
            $mdDialog.show({
               templateUrl: 'Invoicepartials/addCustomer.html',
               controller: function DialogController($scope, $mdDialog) {
                  $scope.addTask = "";
                  $scope.email = "";
                  $scope.baddress = {};
                  $scope.saddress = {};
                  $scope.showShipping = $scope.showShipping;
                  $scope.showBilling = !$scope.showBilling;
                  $scope.closeDialog = function() {
                     $mdDialog.hide();
                  }
                  $scope.addressChange = function() {
                     $scope.showShipping = !$scope.showShipping;
                     $scope.showBilling = !$scope.showBilling;
                  }
                  $scope.AddCus = function() {
                     var client = $objectstore.getClient("contact");
                     client.onComplete(function(data) {
                        $mdDialog.show(
                           $mdDialog.alert()
                           .parent(angular.element(document.body))
                           .title('')
                           .content('Customer Successfully Saved')
                           .ariaLabel('Alert Dialog Demo')
                           .ok('OK')
                           .targetEvent(data)
                        );
                     });
                     client.onError(function(data) {
                        $mdDialog.show(
                           $mdDialog.alert()
                           .parent(angular.element(document.body))
                           .title('Sorry')
                           .content('Error saving Customer')
                           .ariaLabel('Alert Dialog Demo')
                           .ok('OK')
                           .targetEvent(data)
                        );
                     });
                     $scope.contact.customerid = "-999";
                     client.insert([$scope.contact], {
                        KeyProperty: "customerid"
                     });
                     $mdDialog.hide();
                  }
               }
            })
         }
         // end of Add Contact function
      $scope.favouriteFunction = function(obj) {
         var client = $objectstore.getClient("RecurringProfile");
         client.onComplete(function(data) {
            if (obj.favourite) {} else if (!(obj.favourite)) {};
         });
         client.onError(function(data) {
            $mdDialog.show(
               $mdDialog.alert()
               .parent(angular.element(document.body))
               .content('Error Occure while Adding to Favourite')
               .ariaLabel('')
               .ok('OK')
               .targetEvent(data)
            );
         });
         obj.favourite = !obj.favourite;
         client.insert(obj, {
            KeyProperty: "profileName"
         });
      }
      $scope.editContact = function() {
            $mdDialog.show({
               templateUrl: 'Invoicepartials/editCustomer.html',
               controller: function DialogController($scope, $mdDialog) {
                  $scope.addTask = "";
                  $scope.email = "";
                  $scope.baddress = {};
                  $scope.saddress = {};
                  $scope.showShipping = $scope.showShipping;
                  $scope.showBilling = !$scope.showBilling;
                  $scope.addressChange = function() {
                     $scope.showShipping = !$scope.showShipping;
                     $scope.showBilling = !$scope.showBilling;
                  }
                  $scope.closeDialog = function() {
                     $mdDialog.hide();
                  }
                  $scope.editCus = function(cusform) {
                     var client = $objectstore.getClient("contact");
                     client.onComplete(function(data) {
                        $mdDialog.show(
                           $mdDialog.alert()
                           .parent(angular.element(document.body))
                           .title('')
                           .content('invoice Successfully Saved')
                           .ariaLabel('Alert Dialog Demo')
                           .ok('OK')
                           .targetEvent(data)
                        );
                     });
                     client.onError(function(data) {});
                     client.insert(cusform, {
                        KeyProperty: "customerid"
                     });
                  }
               }
            })
         }
         //Autocomplete stuff
      $rootScope.self = this;
      $rootScope.self.tenants = loadAll();
      $rootScope.selectedItem1 = null;
      $rootScope.self.searchText = null;
      $rootScope.self.querySearch = querySearch;

      function querySearch(query) {
         $scope.enter = function(keyEvent) {
               if (keyEvent.which === 13) {
                  if ($rootScope.selectedItem1 === null) {
                     $rootScope.selectedItem1 = query;
                  } else {}
               }
            }
            //Custom Filter
         $rootScope.results = [];
         for (i = 0, len = $scope.customerNames.length; i < len; ++i) {
            if ($scope.customerNames[i].display.indexOf(query) != -1) {
               $rootScope.results.push($scope.customerNames[i]);
            }
         }
         return $rootScope.results;
      }
      $scope.customerNames = [];

      function loadAll() {
         var client = $objectstore.getClient("contact");
         client.onGetMany(function(data) {
            if (data) {
               // $scope.contact =data;
               for (i = 0, len = data.length; i < len; ++i) {
                  $scope.customerNames.push({
                     display: data[i].Name.toLowerCase(),
                     value: data[i],
                     BillingValue: data[i].baddress.street + ', ' + data[i].baddress.city + ', ' + data[i].baddress.zip + ', ' + data[i].baddress.state + ', ' + data[i].baddress.country,
                     shippingValue: data[i].saddress.s_street + ', ' + data[i].saddress.s_city + ', ' + data[i].saddress.s_zip + ', ' + data[i].saddress.s_state + ', ' +
                        data[i].saddress.s_country
                  });
               }
            }
         });
         client.onError(function(data) {});
         client.getByFiltering("*");
      }
      $scope.Billingaddress = true;
      $scope.Shippingaddress = false;
      $scope.changeAddress = function() {
         $scope.Billingaddress = !$scope.Billingaddress;
         $scope.Shippingaddress = !$scope.Shippingaddress;
      }
      $scope.cancel = function() {
         $mdDialog.cancel();
      }
      $scope.saveProduct = function() {
         var client = $objectstore.getClient("12thproduct");
         client.onComplete(function(data) {
            $mdDialog.show(
               $mdDialog.alert()
               .parent(angular.element(document.body))
               .title('')
               .content('product Successfully Saved')
               .ariaLabel('Alert Dialog Demo')
               .ok('OK')
               .targetEvent(data)
            );
         });
         client.onError(function(data) {
            $mdDialog.show(
               $mdDialog.alert()
               .parent(angular.element(document.body))
               .title('Sorry')
               .content('Error saving product')
               .ariaLabel('Alert Dialog Demo')
               .ok('OK')
               .targetEvent(data)
            );
         });
         $scope.tdIinvoice.product_code = "-999";
         client.insert([$scope.tdIinvoice], {
            KeyProperty: "product_code"
         });
      }
     
      $scope.productCode = [];
      //Retrieve product details
      var client = $objectstore.getClient("12thproduct");
      client.onGetMany(function(data) {
         if (data) {
            $scope.product = data;
            return $scope.product;
         }
      });
      client.onError(function(data) {
         $mdDialog.show(
            $mdDialog.alert()
            .parent(angular.element(document.body))
            .title('Sorry')
            .content('There is no products available')
            .ariaLabel('Alert Dialog Demo')
            .ok('OK')
            .targetEvent(data)
         );
      });
      client.getByFiltering("*");

      $scope.viewRec = function() {
               location.href = '#/settings/AllRecurring_Invoices';
         }
         //save invoice details
      $scope.submit = function() {
         $scope.imagearray = UploaderService.loadArray();
         if ($scope.imagearray.length > 0) {
            for (indexx = 0; indexx < $scope.imagearray.length; indexx++) {
               $uploader.upload("45.55.83.253", "recinvoiceUploades", $scope.imagearray[indexx]);
               $uploader.onSuccess(function(e, data) {
                  var toast = $mdToast.simple()
                     .content('Successfully uploaded!')
                     .action('OK')
                     .highlightAction(false)
                     .position("bottom right");
                  $mdToast.show(toast).then(function() {});
               });
               $uploader.onError(function(e, data) {
                  var toast = $mdToast.simple()
                     .content('There was an error, please upload!')
                     .action('OK')
                     .highlightAction(false)
                     .position("bottom right");
                  $mdToast.show(toast).then(function() {
                     //whatever
                  });
               });
            }
         };
         var client = $objectstore.getClient("RecurringProfile");
         $scope.TDinvoice.invoiceProducts = $rootScope.prodArray.val;
         $scope.TDinvoice.total = $scope.total;
         $scope.TDinvoice.finalamount = $scope.famount;
         $scope.TDinvoice.status = "N";
         $scope.TDinvoice.Name = $rootScope.selectedItem1.display;
         $scope.TDinvoice.billingAddress = $rootScope.selectedItem1.BillingValue;
         $scope.TDinvoice.shippingAddress = $rootScope.selectedItem1.shippingValue;
         $scope.TDinvoice.MultiDueDAtesArr = $scope.dateArray.value;
         $scope.TDinvoice.UploadImages = {
            val: []
         };
         $scope.TDinvoice.UploadImages.val = UploaderService.loadBasicArray();
         client.onComplete(function(data) {
            $mdDialog.show(
               $mdDialog.alert()
               .parent(angular.element(document.body))
               .title('')
               .content('invoioce Successfully Saved')
               .ariaLabel('Alert Dialog Demo')
               .ok('OK')
               .targetEvent(data)
            );
         });
         client.onError(function(data) {
            $mdDialog.show(
               $mdDialog.alert()
               .parent(angular.element(document.body))
               .title('Sorry')
               .content('Error saving invoioce')
               .ariaLabel('Alert Dialog Demo')
               .ok('OK')
               .targetEvent(data)
            );
         });
         $scope.TDinvoice.profileName = "-999";
         client.insert([$scope.TDinvoice], {
            KeyProperty: "profileName"
         });
      }
      $scope.draft = function() {
         $scope.imagearray = UploaderService.loadArray();
         if ($scope.imagearray.length > 0) {
            for (indexx = 0; indexx < $scope.imagearray.length; indexx++) {
               $uploader.upload("45.55.83.253", "recinvoiceUploades", $scope.imagearray[indexx]);
               $uploader.onSuccess(function(e, data) {
                  var toast = $mdToast.simple()
                     .content('Successfully uploaded!')
                     .action('OK')
                     .highlightAction(false)
                     .position("bottom right");
                  $mdToast.show(toast).then(function() {});
               });
               $uploader.onError(function(e, data) {
                  var toast = $mdToast.simple()
                     .content('There was an error, please upload!')
                     .action('OK')
                     .highlightAction(false)
                     .position("bottom right");
                  $mdToast.show(toast).then(function() {});
               });
            }
         };
         var client = $objectstore.getClient("RecurringProfileDraft");
         $scope.TDinvoice.invoiceProducts = $rootScope.prodArray.val;
         $scope.TDinvoice.total = $scope.total;
         $scope.TDinvoice.finalamount = $scope.famount;
         $scope.TDinvoice.status = "N";
         $scope.TDinvoice.Name = $rootScope.selectedItem1.display;
         $scope.TDinvoice.billingAddress = $rootScope.selectedItem1.BillingValue;
         $scope.TDinvoice.shippingAddress = $rootScope.selectedItem1.shippingValue;
         $scope.TDinvoice.MultiDueDAtesArr = $scope.dateArray.value;
         $scope.TDinvoice.UploadImages = {
            val: []
         };
         $scope.TDinvoice.UploadImages.val = UploaderService.loadBasicArray();
         client.onComplete(function(data) {
            $mdDialog.show(
               $mdDialog.alert()
               .parent(angular.element(document.body))
               .title('')
               .content('invoioce Successfully Saved')
               .ariaLabel('Alert Dialog Demo')
               .ok('OK')
               .targetEvent(data)
            );
         });
         client.onError(function(data) {
            $mdDialog.show(
               $mdDialog.alert()
               .parent(angular.element(document.body))
               .title('Sorry')
               .content('Error saving invoioce')
               .ariaLabel('Alert Dialog Demo')
               .ok('OK')
               .targetEvent(data)
            );
         });
         $scope.TDinvoice.profileName = "-999";
         client.insert([$scope.TDinvoice], {
            KeyProperty: "profileName"
         });
      }
      $scope.calculatetotal = function() {
         $scope.total = 0;
         angular.forEach($rootScope.prodArray.val, function(tdIinvoice) {
            $scope.total += (((tdIinvoice.price * tdIinvoice.quantity) - ((tdIinvoice.price * tdIinvoice.quantity) * tdIinvoice.discount / 100)) + ((tdIinvoice.price * tdIinvoice.quantity)) * tdIinvoice.tax / 100);
         })
         return $scope.total;
      };
      $scope.finaldiscount = function() {
         $scope.finalDisc = 0;
         $scope.finalDisc = $scope.total - ($scope.total * $scope.TDinvoice.fdiscount / 100);
         return $scope.finalDisc;
      }
      $scope.CalculateTax = function() {
         $scope.salesTax = 0;
         $scope.salesTax = $scope.finalDisc + ($scope.total * $scope.TDinvoice.salesTax / 100);
         return $scope.salesTax;
      }
      $scope.CalculateOtherTax = function() {
         $scope.otherTax = 0;
         $scope.otherTax = $scope.salesTax + ($scope.total * $scope.TDinvoice.anotherTax / 100);
         return $scope.otherTax;
      }
      $scope.finalamount = function() {
         $scope.famount = 0;
         $scope.famount = parseInt($scope.otherTax) + parseInt($scope.TDinvoice.shipping);
         return $scope.famount;
      };
      var client = $objectstore.getClient("contact");
      client.onGetMany(function(data) {
         if (data) {
            $scope.contact = data;
            return $scope.contact;
         }
      });
      client.onError(function(data) {
         $mdDialog.show(
            $mdDialog.alert()
            .parent(angular.element(document.body))
            .title('Sorry')
            .content('There is no products available')
            .ariaLabel('Alert Dialog Demo')
            .ok('OK')
            .targetEvent(data)
         );
      });
      client.getByFiltering("*");
      $scope.DemoCtrl = function($timeout, $q) {
         var self = this;
         self.readonly = false;
         self.fruitNames = [];
         self.TDinvoice.roFruitNames = angular.copy(self.fruitNames);
         self.tags = [];
         self.newVeg = function(chip) {
            return {
               name: chip,
               type: 'unknown'
            };
         };
      }
   })
   //-------------------------------------------------------------------------------------------------------------
   //-------------------------------------------------------------------------------------------------------------
 angular.module('mainApp').controller('ViewRecurring', function($scope, $mdDialog, $objectstore, $window, $rootScope, recurringInvoiceService, $filter, $state, $location) {
      $scope.TDinvoice = {};
      $scope.newItems = [];
      $scope.show = false;
      $scope.showTable = false;
      $scope.obtable = [];
      var vm = this;
      $scope.announceClick = function(index) {
         $mdDialog.show(
            $mdDialog.alert()
            .title('You clicked!')
            .content('You clicked the menu item at index ' + index)
            .ok('ok')
         );
      };
      $scope.sortableOptions = {
         containment: '#sortable-container'
      };
      $rootScope.tenants = loadAll();
      $rootScope.selectedItem2 = null;
      $rootScope.searchText1 = null;
      $rootScope.querySearch1 = querySearch1;

      function querySearch1(query) {
         $scope.enter = function(keyEvent) {
               if (keyEvent.which === 13) {
                  if ($rootScope.selectedItem2 === null) {
                     $rootScope.selectedItem2 = query;
                  } else {}
               }
            }
            //Custom Filter
         $rootScope.results = [];
         for (i = 0, len = $scope.customerNames.length; i < len; ++i) {
            if ($scope.customerNames[i].display.indexOf(query) != -1) {
               $rootScope.results.push($scope.customerNames[i]);
            }
         }
         return $rootScope.results;
      }
      $scope.customerNames = [];

      function loadAll() {
         var client = $objectstore.getClient("RecurringProfile");
         client.onGetMany(function(data) {
            if (data) {
               for (i = 0, len = data.length; i < len; ++i) {
                  $scope.customerNames.push({
                     display: data[i].Name,
                     value: data[i]
                  });
               }
            }
         });
         client.onError(function(data) {
            $mdDialog.show(
               $mdDialog.alert()
               .parent(angular.element(document.body))
               .title('Sorry')
               .content('There is no products available')
               .ariaLabel('Alert Dialog Demo')
               .ok('OK')
               .targetEvent(data)
            );
         });
         client.getByFiltering("*");
      }
      $scope.viewSavedProducts = function(obj) {
         console.log('hit');
         $mdDialog.show({
            templateUrl: 'Invoicepartials/showproduct.html',
            controller: 'productCtrl',
            locals: {
               item: obj
            }
         });
      }
      $scope.onChangeinventory = function(cbState) {
         if (cbState == true) {
            $scope.checkAbility = false;
         } else {
            $scope.checkAbility = true;
         }
      };
      $scope.Billingaddress = true;
      $scope.Shippingaddress = false;
      $scope.changeAddress = function() {
         $scope.Billingaddress = !$scope.Billingaddress;
         $scope.Shippingaddress = !$scope.Shippingaddress;
      }
      $scope.checkAbility = true;
      $scope.onChange = function(cbState) {
         if (cbState == true) {
            $scope.checkAbility = false;
         } else {
            $scope.checkAbility = true;
         }
      };
      $scope.editRecurring = function(editForm) {
         var client = $objectstore.getClient("RecurringProfile");
         $scope.TDinvoice.invoiceProducts = $rootScope.prodArray.val;
         $scope.TDinvoice.total = $scope.total;
         $scope.TDinvoice.finalamount = $scope.famount;
         $scope.TDinvoice.status = "N";
         client.onComplete(function(data) {
            $mdDialog.show(
               $mdDialog.alert()
               .parent(angular.element(document.body))
               .title('')
               .content('invoice Successfully Saved')
               .ariaLabel('Alert Dialog Demo')
               .ok('OK')
               .targetEvent(data)
            );
         });
         client.onError(function(data) {
            $mdDialog.show(
               $mdDialog.alert()
               .parent(angular.element(document.body))
               .title('Sorry')
               .content('Error Saving invoice')
               .ariaLabel('Alert Dialog Demo')
               .ok('OK')
               .targetEvent(data)
            );
         });
         client.insert(editForm, {
            KeyProperty: "profileName"
         });
      }
      $scope.deleteRecurringInvoice = function(deleteform, ev) {
         var confirm = $mdDialog.confirm()
            .parent(angular.element(document.body))
            .title('')
            .content('Are You Sure You Want To Delete This Record?')
            .ok('Delete')
            .cancel('Cancel')
            .targetEvent(ev);
         $mdDialog.show(confirm).then(function() {
            var client = $objectstore.getClient("RecurringProfile");
            client.onComplete(function(data) {
               $mdDialog.show(
                  $mdDialog.alert()
                  .parent(angular.element(document.body))
                  .content('Record Successfully Deleted')
                  .ariaLabel('')
                  .ok('OK')
                  .targetEvent(data)
               );
               $state.go($state.current, {}, {
                  reload: true
               });
            });

            client.onError(function(data) {
               $mdDialog.show(
                  $mdDialog.alert()
                  .parent(angular.element(document.body))
                  .content('Error Deleting Record')
                  .ariaLabel('')
                  .ok('OK')
                  .targetEvent(data)
               );
            });
            client.deleteSingle(deleteform.profileName, "profileName");
         }, function() {
            $mdDialog.hide();
         });
      }
      $scope.calAMount = function(data) {
         $scope.Amount = 0;
         $scope.Amount = (((data.price * data.quantity) - ((data.price * data.quantity) * data.discount / 100)) + ((data.price * data.quantity)) * data.tax / 100);
         return $scope.Amount;
      }
      $scope.calculatetotal = function(data) {
         $scope.total = 0;
         angular.forEach(data.invoiceProducts, function(tdIinvoice) {
            $scope.total += (((tdIinvoice.price * tdIinvoice.quantity) - ((tdIinvoice.price * tdIinvoice.quantity) * tdIinvoice.discount / 100)) + ((tdIinvoice.price * tdIinvoice.quantity)) * tdIinvoice.tax / 100);
         })
         return $scope.total;
      };
      $scope.finaldiscount = function(data) {
         $scope.finalDisc = 0;
         $scope.finalDisc = $scope.total - ($scope.total * data.fdiscount / 100);
         return $scope.finalDisc;
      }
      $scope.CalculateTax = function(data) {
         $scope.salesTax = 0;
         $scope.salesTax = $scope.finalDisc + ($scope.total * data.salesTax / 100);
         return $scope.salesTax;
      }
      $scope.CalculateOtherTax = function(data) {
         $scope.otherTax = 0;
         $scope.otherTax = $scope.salesTax + ($scope.total * data.anotherTax / 100);
         return $scope.otherTax;
      }
      $scope.finalamount = function(data) {
         $scope.famount = 0;
         $scope.famount = parseInt($scope.otherTax) + parseInt(data.shipping);
         return $scope.famount;
      };
      
      $scope.DemoCtrl1 = function($timeout, $q) {
         var self = this;
         self.readonly = false;
         self.invoice.roFruitNames = $scope.invoices.roFruitNames;
         self.newVeg = function(chip) {
            return {
               name: chip,
               type: 'unknown'
            };
         };
      }

      var client = $objectstore.getClient("RecurringProfile");
      client.onGetMany(function(data) {
         if (data) {
            $scope.TDinvoice = data;
         }
      });
      client.onError(function(data) {
         $mdDialog.show(
            $mdDialog.alert()
            .parent(angular.element(document.body))
            .title('This is embarracing')
            .content('There was an error retreving the data.')
            .ariaLabel('Alert Dialog Demo')
            .ok('OK')
            .targetEvent(data)
         );
      });
      client.getByFiltering("*");
      $scope.getSelected = function(inv) {
         $scope.obtable = inv.table;
      }
     
   }) //END OF viewCtrl
//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
 .factory('recurringInvoiceService', function($rootScope) {
      $rootScope.prodArray = {
         val: []
      };
      return {
         setArray: function(newVal) {
            $rootScope.prodArray.val.push(newVal);
            return $rootScope.prodArray;
         },
         removeArray: function(newVals) {
            $rootScope.prodArray.val.splice(newVals, 1);
            return $rootScope.prodArray;
         }
      }
   })
 //----------------------------------------------------------------------------------------------
 //---------------------------------------------------------------------------------------------
 .controller('productCtrl', function($scope, $mdDialog, $rootScope, recurringInvoiceService, item) {
      $scope.test = item;
      $scope.cancel = function() {
         $mdDialog.cancel();
      };
      $scope.edit = function() {
         $mdDialog.cancel();
      };
      $scope.calAMount = function() {
         $scope.Amount = 0;
         $scope.Amount = (((item.price * item.quantity) - ((item.price * item.quantity) * item.discount / 100)) + ((item.price * item.quantity)) * item.tax / 100);
         return $scope.Amount;
      }
   })