angular
    .module('mainApp', ['ngMaterial','directivelibrary','12thdirective','uiMicrokernel','ui.router'])
    .config(function($stateProvider, $urlRouterProvider) {    
  $urlRouterProvider.otherwise('/home');    
  $stateProvider       
       	  .state('home', {
            url: '/home',
           templateUrl: 'product_partials/product_view_partial.html'
        })  
  			.state('Add_Product', {
            url: '/Add_Product',
           templateUrl: 'product_partials/Product_partial.html'
        })
   })

    .directive('fileUpLoadersNew',['$uploader',"$rootScope", "$mdToast",'ProductService', function($uploader,$rootScope, $mdToast, ProductService) {
	  return {
		restrict: 'E',
		template: "<div ng-init='showUploadButton=false;showDeleteButton=false;showUploadTable=false;'><div id='drop-files' ondragover='return false' layout='column' layout-align='space-around center'><div id='uploaded-holder' flex ><div id='dropped-files' ng-show='showUploadTable'><table id='Tabulate' ></table></div></div><div flex ><md-button class='md-raised' id='deletebtn' ng-show='showDeleteButton' class='md-raised' style='color:rgb(244,67,54);margin-left:30px;'><md-icon md-svg-src='img/directive_library/ic_delete_24px.svg'></md-icon></md-button></div><div flex><md-icon md-svg-src='img/directive_library/ic_cloud_upload_24px.svg'></md-icon><text style='font-size:12px;margin-left:10px'>{{label}}<text></div></div></div>",
		scope:{			 
			label:'@',
			uploadType:'@'
		},
		link: function(scope,element){
			// Makes sure the dataTransfer information is sent when we
			// Drop the item in the drop box.
			jQuery.event.props.push('dataTransfer');	

			// file/s on a single drag and drop
			var files;			
			// total of all the files dragged and dropped
			var filesArray = [];
			var sampleArray = [];
			var sampleArraybrochure = [];
			
			// Bind the drop event to the dropzone.
			element.find("#drop-files").bind('drop', function(e) {
					
				// Stop the default action, which is to redirect the page
				// To the dropped file
				
				  files = e.dataTransfer.files || e.dataTransfer.files;
				
				if (scope.uploadType == "image") {
				  for(indexx = 0; indexx < files.length; indexx++) {
						filesArray.push(files[indexx]);
						ProductService.setArray(files[indexx]);
						ProductService.BasicArray(filesArray[indexx].name,filesArray[indexx].size);
						sampleArray.push({'name': filesArray[indexx].name, 'size': filesArray[indexx].size});						 
					}

				}else if (scope.uploadType == "brochure") {

					 for(indexx = 0; indexx < files.length; indexx++) {
						filesArray.push(files[indexx]);
						ProductService.setArraybrochure(files[indexx]);
						ProductService.BasicArraybrochure(filesArray[indexx].name,filesArray[indexx].size);
						sampleArraybrochure.push({'name': filesArray[indexx].name, 'size': filesArray[indexx].size});						 
					}

				};
				

			 var newHtml = "<tr class='md-table-headers-row'><th class='md-table-header' style='Padding:0px 16px 10px 0'>Name</th><th class='md-table-header' style='Padding:0px 16px 10px 0'>Type</th><th class='md-table-header' style='Padding:0px 16px 10px 0'>Size</th></tr>";

			  for (var i = 0; i < filesArray.length; i++) {
					 var tableRow = "<tr><td class='upload-table' style='float:left'>" + filesArray[i].name + "</td><td class='upload-table'>" +
					 filesArray[i].type+ "</td><td class='upload-table'>" +
					 filesArray[i].size +" bytes"+ "</td></tr>";
					 newHtml += tableRow;
				}
				
				element.find("#Tabulate").html(newHtml);
				 
				 $rootScope.$apply(function(){
					scope.showUploadButton = true;
					scope.showDeleteButton = true;
					scope.showUploadTable = true;
				 })
	
			});
			
			function restartFiles() {
				
				// We need to remove all the images and li elements as
				// appropriate. We'll also make the upload button disappear
				
				
				
				 $rootScope.$apply(function(){
					scope.showUploadButton = false;
					scope.showDeleteButton = false;
					scope.showUploadTable = false;
				 })
			
				// And finally, empty the array
				ProductService.removeArray(filesArray,scope.uploadType);
				ProductService.removebasicArray(sampleArray);
				ProductService.removebasicArraybrochure(sampleArraybrochure);
				filesArray = [];

				
				return false;
			}
			
		 
		
			
			// Just some styling for the drop file container.
			element.find('#drop-files').bind('dragenter', function() {
				$(this).css({'box-shadow' : 'inset 0px 0px 20px rgba(0, 0, 0, 0.1)', 'border' : '2px dashed rgb(255,64,129)'});
				return false;
			});
			
			element.find('#drop-files').bind('drop', function() {
				$(this).css({'box-shadow' : 'none', 'border' : '2px dashed rgba(0,0,0,0.2)'});
				return false;
			});
			
		
			element.find('#deletebtn').click(restartFiles);
			
		
		} //end of link
	  };
	}])
	
	.factory('ProductService', function($rootScope){
		  $rootScope.testArray = [];
		  $rootScope.basicinfo = [];
		  $rootScope.testArraybrochure = [];
		  $rootScope.basicinfobrochure = [];
  return {

  	 setArraybrochure: function(newVal) {
        $rootScope.testArraybrochure.push(newVal);
     	return $rootScope.testArraybrochure;
    },
  loadArraybrochure: function() {    
	    return $rootScope.testArraybrochure;
   },
   loadBasicArraybrochure: function() {    
	    return $rootScope.basicinfobrochure;
   },
  BasicArraybrochure: function(name,size) { 

		$rootScope.basicinfobrochure.push({'name': name , 'size': size});
   		console.log($rootScope.basicinfobrochure);
     	return $rootScope.basicinfobrochure;
   },
  
  setArray: function(newVal) {
        $rootScope.testArray.push(newVal);
     	return $rootScope.testArray;
    },
  loadArray: function() {    
	    return $rootScope.testArray;
   },
   loadBasicArray: function() {    
	    return $rootScope.basicinfo;
   },
  BasicArray: function(name,size) { 

		$rootScope.basicinfo.push({'name': name , 'size': size});
   		console.log($rootScope.basicinfo);
     	return $rootScope.basicinfo;
   },
  removeArray: function(arr,type){

	  	if (type == "image") {
		  	for (var i = arr.length - 1; i >= 0; i--) {
		   	$rootScope.testArray.splice(arr[i], 1);
		   	};
		   	console.log($rootScope.testArray);
		   	return $rootScope.testArray;

	   }
	   else if (type == "brochure") {

	   		for (var i = arr.length - 1; i >= 0; i--) {
		   	$rootScope.testArraybrochure.splice(arr[i], 1);
		   	};
		   	console.log($rootScope.testArraybrochure);
		   	return $rootScope.testArraybrochure;
	   };

   	
   },

   removebasicArraybrochure: function(arr){

   		 	for (var i = arr.length - 1; i >= 0; i--) {

		   		$rootScope.basicinfobrochure.splice(arr[i], 1);

			   	};
			   	console.log($rootScope.basicinfobrochure);
			   	return $rootScope.basicinfobrochure;

	 },
   removebasicArray: function(arr){
	 
		   	for (var i = arr.length - 1; i >= 0; i--) {
		   		$rootScope.basicinfo.splice(arr[i], 1);
		   	};
		   	console.log($rootScope.basicinfo);
		   	return $rootScope.basicinfo;
		
   }

  }    
  
})
   
   .controller('AppCtrl', function ($scope,ProductService,$uploader,$mdDialog,$state, $mdToast,$objectstore,mfbDefaultValues,$window,$rootScope,$interval,$location) {
   

   // sort function variable start


        $scope.testarr = [
                         {name:"Starred",id:"favouriteStarNo", src:"img/ic_grade_48px.svg",divider:true },
                         {name:"Date",id:"todaydate", src:"img/ic_add_shopping_cart_48px.svg",divider:false},                         
                         {name:"Product Name",id:"Productname",  src:"img/ic_add_shopping_cart_48px.svg",divider:false},
                         {name:"Code",id:"ProductCode",  src:"img/ic_add_shopping_cart_48px.svg",divider:false},                         
                         {name:"Brand",id:"brand",  src:"img/ic_add_shopping_cart_48px.svg", divider:false},
                         {name:"Catergory",id:"ProductCategory",  src:"img/ic_add_shopping_cart_48px.svg",divider:true},
                         {name:"Status 1",id:"Status",  src:"img/ic_add_shopping_cart_48px.svg",divider:false}
                         ]
 
         $rootScope.prodSearch = "";
		 $scope.self = this;
		 $scope.self.searchText = "";

      
        //sort function variable end 

   	$scope.brochureuploader = function(){
		$mdDialog.show({
	        templateUrl: 'product_partials/brochure_dialog_partial.html',
	        controller: EyeController                 
	    })
	}


		$scope.imageuploader = function(){
		$mdDialog.show({
	        templateUrl: 'product_partials/image_dialog_partial.html',
	        controller: EyeController                 
	    })
	}

	 function EyeController($scope, $mdDialog, $rootScope,$state) {

	 	$scope.AddCus = function(){
	 		$mdDialog.hide();
	 	}
	 		$scope.closeDialog = function(){
	 		$mdDialog.hide();
	 	}

 }

   $scope.stockdisabled = false;
   $scope.stockdisabledview = false;
 
   $scope.categorytype = "Product"
   $scope.progresshow = false;
   $scope.progressbrochure = false;
   $scope.progresshowfull = false;
 //  $scope.stockdisabledpackageview = false;	 
   $scope.securityKey = '';
   $scope.toggleSearch = false;  
   $scope.checkAbility = true; 
   $scope.checkAbilityproduct = true;
   $scope.products=[];
  // $scope.packages=[];
   $scope.loadallarray = [];





   // sort function variable start
   $scope.prodSearch = "";
   $scope.sortProdName = "Productname";
   $scope.sortProdCode = "ProductCode";
   $scope.sortProdBrand = "brand";
   $scope.sortProdCat= "ProductCategory";
   $scope.sortProdStatus = "status";
   $scope.sortProdStock = "stocklevel";
   $scope.mytest = null;

   $scope.sortFunciton = function(name){
   	
   	$scope.prodSearch = name;
   	self.searchText = null;
   	console.log($scope.prodSearch);

   }

   $scope.testfunc = function(){
   		self.searchText = "true"
   }

      //sort function variable end 

  				var self = this;
				// list of `state` value/display objects
				self.tenants       = loadAll();
				self.selectedItem  = null;
				self.searchText    = null;
				self.querySearch   = querySearch;
		 
				// ******************************
				// Internal methods
				// ******************************
				/**
				 * Search for tenants... use $timeout to simulate
				 * remote dataservice call.
				 */

				 function querySearch (query) {

				 	$scope.enter = function(keyEvent) {
				 		if (keyEvent.which === 13)
				 		{	
				 			if(self.selectedItem === null)
				 			{
				 				self.selectedItem = query;	
				 				console.log(results);
				 		
				 			}else{
				 				console.log(self.selectedItem);

				 			}
				 		}
				 	}
				 
				}

				function loadAll(){}

function setroute(route){

	$location.path(route);


}

$scope.favouriteFunction = function(obj){
		var client = $objectstore.getClient("12thproduct");		
		 	client.onComplete(function(data){ 	 

		 		if (obj.favouriteStar) {
		 				 
		 		var toast = $mdToast.simple()
				  .content('Add To Favourite')
				  .action('OK')
				  .highlightAction(false)
				  .position("bottom right");
			$mdToast.show(toast).then(function() {
				//whatever
			});
		 		}else if (!(obj.favouriteStar)) {

		 		var toast = $mdToast.simple()
					  .content('Remove from Favourite')
					  .action('OK')
					  .highlightAction(false)
					  .position("bottom right");
				$mdToast.show(toast).then(function() {
					//whatever
				});
		 		};
			
			});

			client.onError(function(data){				 
				var toast = $mdToast.simple()
					  .content('Error Occure while Adding to Favourite')
					  .action('OK')
					  .highlightAction(false)
					  .position("bottom right");
				$mdToast.show(toast).then(function() {
					//whatever
				});

			});	

		if (obj.favouriteStarNo == 1 ) {
			obj.favouriteStarNo = 0;
		}
		else if (obj.favouriteStarNo == 0){
			obj.favouriteStarNo = 1;
		};

		obj.favouriteStar = !obj.favouriteStar;		         
        client.insert(obj, {KeyProperty:"product_code"});

	
}
 

$scope.onChangeproduct = function(cbState) { 
	if(cbState == true)
	{
		$scope.checkAbilityproduct = false;
		$scope.checkAbility = true;

	}else
	{
		$scope.checkAbilityproduct = true;
		$scope.checkAbility = false;

	}
};

 


$scope.changeinventoryview = function(type){
		if (type == "1") {
			$scope.stockdisabledview = true;

		}
		else if (type == "2") {
			$scope.stockdisabledview = false;
		};
	}

	$scope.changeinventory = function(type){

		if (type == "1") {
			$scope.stockdisabled = true;
			$scope.product.stocklevel = null;
		}
		else if (type == "2") {
			$scope.stockdisabled = false;
		};



	}

	$scope.categorychange = function(type){

		if (type == "Product") {

			$scope.categorytype = "Product";
		}

		else if (type == "Package") {

			$scope.categorytype = "Package";
		};

	}
	

	//The two buttons click events below are defined in the rejectContentTemplate
	$scope.hideReject = function() {
		$mdDialog.hide($scope.securityKey);
	};
	
	$scope.cancelReject = function() {
		$mdDialog.cancel();
	};
	
	$scope.statusIncludes = [];
		
	$scope.includeStatus = function(Status) {
		var i = $.inArray(Status, $scope.statusIncludes);
		if (i > -1) {
			$scope.statusIncludes.splice(i, 1);
			
		} else {
			$scope.statusIncludes.push(Status);
			
		}
		
	}
	
	$scope.statusFilter = function(userDetails) {
		if ($scope.statusIncludes.length > 0) {
			if ($.inArray(userDetails.Status, $scope.statusIncludes) < 0)
			
				return;
		}
		
		return userDetails;
	}

	$scope.scrollbarConfig = {
			autoHideScrollbar: false,
			theme: 'minimal-dark',
			axis: 'y',
			advanced: {
			updateOnContentResize: true
			},
			scrollInertia: 300
		}
   
		$scope.hideProductDetails = function()
		{
			alert("what");
		}
 
 
		         
				
				 var len;
				
				 $scope.loadAllProducts=function(){

				 	 $scope.loadallarray = [];
					 $scope.products=[];
					 $scope.packages=[];
					 var client = $objectstore.getClient("12thproduct");
					client.onGetMany(function(data){
						if (data){

							
							 $scope.loadallarray = data;
							  $scope.products=data;
						 					 
							 						 
						}

					});	
					client.onError(function(data){
						$mdDialog.show(
							$mdDialog.alert()
						   .parent(angular.element(document.body))
						   .title('This is embarracing')
						   .content('There was an error retreving the data.')
						   .ariaLabel('Alert Dialog Demo')
						   .ok('OK')
						   .targetEvent(data)
						);
					});

					client.getByFiltering("*");
					
				};

				$scope.content1 = {val: []};
				$scope.content2 = {val: []};

 

				$scope.getuploaddata = function(key,type,stock){					 

					if (type == "1") {

						//$scope.stockdisabledpackageview = true;
						$scope.stockdisabledproductview = true;
					}

					else if (type == "2") {

						//$scope.stockdisabledpackageview = false;
						$scope.stockdisabledproductview = false;
					};

			 
				 
				}

 
			$scope.changeinventoryproductview = function(type,prod){


				if (type == "1") {

					$scope.stockdisabledproductview = true;
					prod.stocklevel = "";
			 

				}

				if (type == "2") {

					$scope.stockdisabledproductview = false;
					 
				};



			}
 
		function mainAction() {
		  
		}

		function setMainAction() {
			if(vm.chosen.action === 'fire') {
				vm.mainAction = mainAction;
			} else {
				vm.mainAction = null;
			}
		}
		
		$scope.newItems=[];
		$scope.product={};

		$scope.submit=function(){


            var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth()+1; //January is 0!
			var yyyy = today.getFullYear();

			if(dd<10) {
			    dd='0'+dd
			} 

			if(mm<10) {
			    mm='0'+mm
			} 

			today = mm+'/'+dd+'/'+yyyy;


			$scope.imagearray = [];
			$scope.imagearray = ProductService.loadArray();
			$scope.brochurearray = ProductService.loadArraybrochure();
			console.log($scope.imagearray);


			if ($scope.imagearray.length > 0) {

				for	(indexx = 0; indexx < $scope.imagearray.length; indexx++) {
							console.log($scope.imagearray[indexx].name);
							

							$uploader.upload("45.55.83.253","productimagesNew", $scope.imagearray[indexx]);
							$uploader.onSuccess(function(e,data){

								var toast = $mdToast.simple()
									  .content('Successfully uploaded!')
									  .action('OK')
									  .highlightAction(false)
									  .position("bottom right");
								$mdToast.show(toast).then(function() {
									//whatever
								});
							});

							$uploader.onError(function(e,data){
							
								var toast = $mdToast.simple()
										  .content('There was an error, please upload!')
										  .action('OK')
										  .highlightAction(false)
										  .position("bottom right");
									$mdToast.show(toast).then(function() {
										//whatever
									});
							});
							
					}


			};

			 if ($scope.brochurearray.length > 0) {

				for	(indexx = 0; indexx < $scope.brochurearray.length; indexx++) {
							console.log($scope.brochurearray[indexx].name);
							

							$uploader.upload("45.55.83.253","productbrochureNew", $scope.brochurearray[indexx]);
							$uploader.onSuccess(function(e,data){

								var toast = $mdToast.simple()
									  .content('Successfully uploaded!')
									  .action('OK')
									  .highlightAction(false)
									  .position("bottom right");
								$mdToast.show(toast).then(function() {
									//whatever
								});
							});

							$uploader.onError(function(e,data){
							
								var toast = $mdToast.simple()
										  .content('There was an error, please upload!')
										  .action('OK')
										  .highlightAction(false)
										  .position("bottom right");
									$mdToast.show(toast).then(function() {
										//whatever
									});
							});
							
					}


			};


			var client = $objectstore.getClient("12thproduct");
			client.onComplete(function(data){
			$mdDialog.show(
							$mdDialog.alert()
						   .parent(angular.element(document.body))
						   //.title('This is embarracing')
						   .content('Product Successfully Saved.')
						   .ariaLabel('Alert Dialog Demo')
						   .ok('OK')
						   .targetEvent(data)
						);
            $scope.newItems.push($scope.product);  
				});
        
			client.onError(function(data){
			$mdDialog.show(
							$mdDialog.alert()
						   .parent(angular.element(document.body))
						   .content('There was an error saving the data.')
						   .ariaLabel('Alert Dialog Demo')
						   .ok('OK')
						   .targetEvent(data)
						);
				
			});

			$scope.product.progressshow = "false"
			$scope.product.product_code = "-999"
			$scope.product.favouriteStar = false;
			$scope.product.favouriteStarNo = 1;
			$scope.product.todaydate = today;
			$scope.product.UploadImages = {val: []};
			$scope.product.UploadBrochure = {val: []};
			$scope.product.UploadImages.val = ProductService.loadBasicArray();
			$scope.product.UploadBrochure.val = ProductService.loadBasicArraybrochure();

			console.log($scope.product)
			client.insert($scope.product, {KeyProperty:"product_code"});
		}



		$scope.updateproduct = function(updatedForm,prod){


				var client = $objectstore.getClient("12thproduct");
				 
				 console.log(updatedForm.stocklevel);

			client.onComplete(function(data){ 
				$scope.newItems.push($scope.product);

				$mdDialog.show(
					$mdDialog.alert()
					.parent(angular.element(document.body))
			         //.title('This is embarracing')
			         .content('Product Successfully Updated')
			         .ariaLabel('')
			         .ok('OK')
			         .targetEvent(data)			         
			         );
			});

			client.onError(function(data){
				$mdDialog.show(
					$mdDialog.alert()
					.parent(angular.element(document.body))
			         //.title('This is embarracing')
			         .content('Error Updating Product')
			         .ariaLabel('')
			         .ok('OK')
			         .targetEvent(data)
			         );

			});			         
        client.insert(updatedForm, {KeyProperty:"product_code"});

		}




		$scope.deleteproduct = function(deleteform){

    	var client = $objectstore.getClient("12thproduct");
           client.onComplete(function(data){ 
 
				$mdDialog.show(
					$mdDialog.alert()
					.parent(angular.element(document.body))
			         //.title('This is embarracing')
			         .content('Product Successfully Deleted')
			         .ariaLabel('')
			         .ok('OK')
			         .targetEvent(data)			         
			         );

				location.href = '#/home';

	 

			});

			client.onError(function(data){
				$mdDialog.show(
					$mdDialog.alert()
					.parent(angular.element(document.body))
			         //.title('This is embarracing')
			         .content('Error Deleting Product')
			         
			         .ariaLabel('')
			         .ok('OK')
			         .targetEvent(data)
			         );

			});		 

			        client.deleteSingle(deleteform.product_code,"promotion_code");
	}    
 
		$scope.demo = {
        topDirections: ['left', 'up'],
        bottomDirections: ['down', 'right'],
        isOpen: false,
        availableModes: ['md-fling', 'md-scale'],
        selectedMode: 'md-fling',
        availableDirections: ['up', 'down', 'left', 'right'],
        selectedDirection: 'up'
      };		



			$scope.save=function(){

				setTimeout(function(){
						$('#mySignup').click(); 

				},0)
				
			}	  
			$scope.viewProduct =function(){
				   // $('#viewProduct').animate({width:"100%",height:"100%", borderRadius:"0px", right:"0px", bottom:"0px", opacity: 0.25},400, function() { 
     		// 		  // $window.location="product_view.html";
     		// 		  	// setroute('home');	
     		// 		    location.href = '#/home';
    			//   });

				      location.href = '#/home';
			
			}
       
			$scope.addProduct =function(){
				  $('#addProduct').animate({width:"100%",height:"100%", borderRadius:"0px", right:"0px", bottom:"0px", opacity: 0.25},400, function() { 
     				  // $window.location="index.html";
     				  	// setroute('Add_Product');	
     				  		  location.href = '#/Add_Product';
    			  });
			}



			      $scope.TDinvoice = {};
 
	  
	$rootScope.$on('viewRecord', function(event, args) {

	$scope.imageDetails = args;

		 var fileExt = args.name.split('.').pop()

		 console.log(args.name);

		 console.log(fileExt);


		 if (fileExt == "docx") {

		 		$scope.progressbrochure = true;

		 	var client = $objectstore.getClient("productbrochure");
		 	client.onGetMany(function(data){
		 		if (data){
		 			$scope.brochurebody = [];

		 			$scope.brochurebody = data;
		 			var pbody = data

		 			console.log(pbody);

		 			for (var i = $scope.brochurebody.length - 1; i >= 0; i--) {
		 				var url = 'data:application/msword;base64,' + $scope.brochurebody[i].Body;
		 				window.location.href = url;
		 			};


		 				$scope.progressbrochure = false;
		 		}



		 	});	
		 	client.onError(function(data){
		 		$mdDialog.show(
		 			$mdDialog.alert()
		 			.parent(angular.element(document.body))
		 			.title('This is embarracing')
		 			.content('There was an error Downloading the Document.')
		 			.ariaLabel('Alert Dialog Demo')
		 			.ok('OK')
		 			.targetEvent(data)
		 			);

		 			$scope.progressbrochure = false;
		 	});

		 	client.getByFiltering("select Body from productbrochureNew where FileName like '%"+args.name+"'%");

		 }

		 else if (fileExt == "pdf") {


		 		$scope.progressbrochure = true;

		 	var client = $objectstore.getClient("productbrochure");
		 	client.onGetMany(function(data){
		 		if (data){

		 			$scope.brochurebodypdf = [];

		 			$scope.brochurebodypdf = data;

		 			console.log($scope.brochurebodypdf);

		 			for (var i = $scope.brochurebodypdf.length - 1; i >= 0; i--) {

		 				var url = 'data:application/pdf;base64,' + $scope.brochurebodypdf[i].Body;
		 				window.location.href = url;

		 			};

		 				$scope.progressbrochure = false;
		 		}
		 	});	
		 	client.onError(function(data){
		 		$mdDialog.show(
		 			$mdDialog.alert()
		 			.parent(angular.element(document.body))
		 			.title('This is embarracing')
		 			.content('There was an error Loading the PDF file .')
		 			.ariaLabel('Alert Dialog Demo')
		 			.ok('OK')
		 			.targetEvent(data)
		 			);

		 			$scope.progressbrochure = false;
		 	});

		 	client.getByFiltering("select Body from productbrochureNew where FileName like '%"+args.name+"'%");

		 }


		 else if (fileExt == "png" || fileExt == "jpg") {

		 			$scope.progresshow = true;

		 	var client = $objectstore.getClient("productimages");
		 	client.onGetMany(function(data){
		 		if (data){

		 		$scope.imageload = [];
		 			$scope.imageload = data;
		 			console.log($scope.imageload);
		 			for (var i = $scope.imageload.length - 1; i >= 0; i--) {

		 				var url = 'data:image/png;base64,' + $scope.imageload[i].Body;
		 				window.location.href = url;

		 			};
		 					$scope.progresshow = false;
		 		}
		 	});	
		 	client.onError(function(data){
		 		$mdDialog.show(
		 			$mdDialog.alert()
		 			.parent(angular.element(document.body))
		 			.title('This is embarracing')
		 			.content('There was an error Loading the image file .')
		 			.ariaLabel('Alert Dialog Demo')
		 			.ok('OK')
		 			.targetEvent(data)
		 			);

		 				$scope.progresshow = false;
		 	});

		 	client.getByFiltering("select Body from productimagesNew where FileName like '%"+args.name+"'%");
		 		// $mdDialog.show({
     //      template:acceptContentTemplate,
     //      controller: 'testCtrl',
     //      locals: { employee: $scope.imageDetails }
     //    });
		 }

		 else
		 {

		 	$mdDialog.show(
		 		$mdDialog.alert()
		 		.parent(angular.element(document.body))
		 		.title('Invalid File Format')
		 		.content('This is a invalid file format, please upload only png,docx,jpg and pdf file types')
		 		.ariaLabel()
		 		.ok('Ok')
		 		.targetEvent(event)
		 		);
		 }
  });
  
   $scope.toggleSearch = false;   
  $scope.headers = [
    {
      name: 'Name', 
      field: 'name'
    },{
      name:'Size', 
      field: 'size'
    } 
  ];

  $scope.custom = {name: 'bold', size:'grey'};
  $scope.sortable = ['name', 'size'];
  $scope.thumbs = 'thumb';
  $scope.count = 3;
  
   var acceptContentTemplate = '\
          <md-dialog>\
          <md-dialog-content style="padding:24px;">\
          <div layout layout-sm="column" layout-margin>\
           <div flex="5">\
            <img src="img/material alperbert/avatar_tile_f_28.png" style="margin-top:22px;border-radius:20px"/>\
            </div>\
            <div flex="30" style="margin-top:35px;">\
             <label style="font-weight:700"></label>\
              </div>\
              	 <img data-ng-src="data:image/png;base64,{{test.Body}}" data-err-src="img/avatar.png" style="width:200px; height:100px;/>\
             </div>\
         <div style="margin-left:120px;">\
        </div></br><br>\
      <md-divider></md-divider>\
      <div class="md-actions"> \
              <md-button class="md-primary md-button md-default-theme" ng-click="cancelAccept()"></md-button> \
            <md-button class="md-primary md-button md-default-theme" ng-click="hideAccept()">OK</md-button> \
          </div> \
          </md-content> \
        </md-dialog> ';


		

   })//END OF AppCtrl
   
    .controller('testCtrl', function($scope,employee,$mdDialog){
              
	    $scope.test = employee;
    
	   $scope.hideAccept = function() {
    $mdDialog.hide();
  };
  
  $scope.cancelAccept = function() {
    $mdDialog.cancel();
  };
   })


   function containsObject(obj, list) {

            for (var i = 0; i < list.length; i++) {
                if (list[i].Id == obj.Id) {
                    return true;
                }
            }

            return false;
        }