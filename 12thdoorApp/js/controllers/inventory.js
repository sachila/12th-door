angular
    .module('mainApp', ['ngMaterial','directivelibrary','12thdirective', 'uiMicrokernel','ui.router','ui.sortable','ngAnimate'])
    // ui route config start
    .config(function($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/home/receipt');
        $stateProvider
            .state('home', {
            url: '/home',
            templateUrl: 'inventory_partials/Inventory_view_partial.html',
            controller: 'viewctrl'

        }).state('home.issue', {
            url: '/issue',
            templateUrl: 'inventory_partials/issue_view_partial_child.html',
            controller: 'viewctrl'

        }).state('home.receipt', {
            url: '/receipt',
            templateUrl: 'inventory_partials/receipt_view_partial_child.html',
            controller: 'viewctrl'

        }).state('Add_Inventory', {
            url: '/Add_Inventory',
            templateUrl: 'inventory_partials/Inventory_new_receipt.html',
            controller: 'AppCtrl'

        })
        .state('Add_Inventory_Issue', {
            url: '/Add_Inventory_Issue',
            templateUrl: 'inventory_partials/Inventory_new_issue.html',
            controller: 'AppCtrl'

        })
    })
//ui route config end
.config(function($mdThemingProvider) {
    $mdThemingProvider.theme('datePickerTheme').primaryPalette('teal');
})
.controller('AppCtrl', function($scope,$location, dialogsvc,InvoiceService, $mdToast, $rootScope, $state, $objectstore, $mdDialog, mfbDefaultValues, $window, $objectstore, $auth, $q, $http, $compile, $timeout) {
      

    //upload controllers start

        $scope.imageuploader = function(){
        $mdDialog.show({
            templateUrl: 'inventory_partials/image_dialog_partial.html',
            controller: EyeController                 
        })
    }

     function EyeController($scope, $mdDialog, $rootScope,$state) {

        $scope.AddCus = function(){
            $mdDialog.hide();
        }
            $scope.closeDialog = function(){
            $mdDialog.hide();
        }

 }


    //upload controllers end 



          $scope.sortableOptions = {
            containment: '#sortable-container'
          };


          //delete product start
          $scope.deleteproduct = function(arr, index){       
               arr.splice(index, 1);
          }

          //delete product end 

          //chips start 

               $scope.DemoCtrl = function ($timeout, $q) {
        var self = this;
        self.readonly = false;
        // Lists of tags names and Vegetable objects
        self.fruitNames = [];
        self.inventory.roFruitNames = angular.copy(self.fruitNames);
        self.tags = [];   

      self.newVeg = function(chip) {
          return {
            name: chip,
            type: 'unknown'
          };
        };
      }

          //chips end

        // address input icon functions start
        $scope.addresshow = true;
        $scope.shipaddresshow = false;
        $rootScope.inventoryType = "insert"; 
        $scope.Address = "Address";
        $scope.AddressCount = false;

         

                
        $scope.addressfuncold = function() {

            if ($scope.Address == "Address") {
                $scope.Address = "Shipping Address";
                $scope.AddressCount = true;
                $scope.addresshow = false;
                $scope.shipaddresshow = true;

            } else if ($scope.Address == "Shipping Address") {
                $scope.Address = "Address";
                $scope.AddressCount = false;
                $scope.addresshow = true;
                $scope.shipaddresshow = false;
            };
        }
        //save functon start
        $scope.inventory = {};

        $scope.submit = function() {

            console.log(self.selectedItem)
                var client = $objectstore.getClient("inventory12thdoor");
                client.onComplete(function(data) {
                    $mdDialog.show(
                        $mdDialog.alert()
                        .parent(angular.element(document.body))
                        //.title('This is embarracing')
                        .content('Inventory Successfully Saved.')
                        .ariaLabel('Alert Dialog Demo')
                        .ok('OK')
                        .targetEvent(data)
                    );
                });

                client.onError(function(data) {
                    $mdDialog.show(
                        $mdDialog.alert()
                        .parent(angular.element(document.body))
                        .content('There was an error saving the data.')
                        .ariaLabel('Alert Dialog Demo')
                        .ok('OK')
                        .targetEvent(data)
                    );
                });

                if ($state.current.name == 'Add_Inventory') {
                    $scope.inventory.inventoryClass = "Receipt";
                }else if ($state.current.name == 'Add_Inventory_Issue') {
                    $scope.inventory.inventoryClass = "Issue";
                }


                $scope.inventory.inventoryFavourite = false;
                $scope.inventory.favouriteStarNo = 1;                
                $scope.inventory.customerNames = self.selectedItem.display;
                $scope.inventory.AddressName = $scope.Address;
                $scope.inventory.BillAddress = self.selectedItem.BillAddress;
                $scope.inventory.ShipAddress = self.selectedItem.ShipAddress;
                $scope.inventory.inventory_code = "-999";
                $scope.inventory.itemdetails = $rootScope.testArray.val;
                client.insert($scope.inventory, {
                    KeyProperty: "inventory_code"
                });

            }
            //save function end   
     
        //fab button functions
          $scope.demo = {
            topDirections: ['left', 'up'],
            bottomDirections: ['down', 'right'],
            isOpen: false,
            availableModes: ['md-fling', 'md-scale'],
            selectedMode: 'md-fling',
            availableDirections: ['up', 'down', 'left', 'right'],
            selectedDirection: 'up'
        };
        
        $scope.types = "Receipt";

        $scope.save = function() {
            $('#mySignup').click();
        }
        
        $scope.viewInventory = function() {

            $rootScope.inventoryType = "cards";
            $rootScope.selectedIndex = 0;
            $rootScope.showaddInventory = true;
            location.href = '#/home/receipt';

            // $('#viewinventory').animate({
            //     width: "100%",
            //     height: "100%",
            //     borderRadius: "0px",
            //     right: "0px",
            //     bottom: "0px",
            //     opacity: 0.25
            // }, 400, function() {
            //     location.href = '#/home/receipt';
            // });
        }

        $scope.viewIssue = function() {

            $rootScope.inventoryType = "cards";
            $rootScope.selectedIndex = 1;
            $rootScope.showaddInventory = false;
            location.href = '#/home/issue';

            // $('#viewissues').animate({
            //     width: "100%",
            //     height: "100%",
            //     borderRadius: "0px",
            //     right: "0px",
            //     bottom: "0px",
            //     opacity: 0.25
            // }, 400, function() {
            //     location.href = '#/home/issue';
            // });
        }   
        //end of fab button functions

        // table add button function start
        $scope.addproduct = function() {
           $rootScope.plusdialog = false;
           dialogsvc.addmethod();
        }  
        $scope.addproductreceipt = function() {
           $rootScope.plusdialog = true;
           dialogsvc.addmethod();
        }      
        // table add button function end

        
        // table ng click start
        $rootScope.tableContent = [];

        $scope.tableclick = function(arr,obj) {
            console.log(arr)
            $rootScope.tableContent = [];
            $rootScope.tableContent = arr;
            dialogsvc.method(obj);
        }

        $scope.tableclickadd = function(obj){
            dialogsvc.method(obj);
        }
        //table ng click end


        var self = this;      
        self.tenants = loadAll();
        self.selectedItem = null;
        self.searchText = null;
        self.querySearch = querySearch;
        self.querySearchView = querySearchView;

        function querySearch(query) {
            $scope.enter = function(keyEvent) {
                if (keyEvent.which === 13) {
                    if (self.selectedItem === null) {
                        self.selectedItem = query;
                        console.log(results);
                    } else {
                        console.log(self.selectedItem);
                    }
                }
            }
            //Custom Filter
            var results = [];
            for (i = 0, len = $scope.customerNames.length; i < len; ++i) {
                if ($scope.customerNames[i].display.indexOf(query) != -1) {
                    results.push($scope.customerNames[i]);
                }
            }

            return results;
        }
        function querySearchView(query) {
            $scope.enter = function(keyEvent) {
                if (keyEvent.which === 13) {
                    if (self.selectedItem === null) {
                        self.selectedItem = query;
                        console.log(results);
                    } else {
                        console.log(self.selectedItem);
                    }
                }
            }
        }
        $scope.customerNames = [];

        function loadAll() {
            var client = $objectstore.getClient("contact");
            client.onGetMany(function(data) {
                if (data) {
                     for (i = 0, len = data.length; i < len; ++i) {
                        $scope.customerNames.push({
                            display: data[i].CustomerFname + ' ' + data[i].CustomerLname,
                            BillAddress: data[i].baddress.street + ', ' + data[i].baddress.city + ', ' + data[i].baddress.zip + ', ' +
                                data[i].baddress.country,
                            ShipAddress: data[i].saddress.s_city + ', ' + data[i].saddress.s_country + ', ' + data[i].saddress.s_zip + ', ' +
                                data[i].saddress.s_country
                        });
                    }
                  }
            });
            client.onError(function(data) {
                $mdDialog.show(
                    $mdDialog.alert()
                    .parent(angular.element(document.body))
                    .title('Sorry')
                    .content('There is no products available')
                    .ariaLabel('Alert Dialog Demo')
                    .ok('OK')
                    .targetEvent(data)
                );
            });
            client.getByFiltering("*");
        }        

    }) //END OF AppCtrl

.controller('viewctrl', function($scope,$location, dialogsvc,InvoiceService, $mdToast, $rootScope, $state, $objectstore, $mdDialog, mfbDefaultValues, $window, $objectstore, $auth, $q, $http, $compile, $timeout){


    $scope.testdialog = function(){
        $mdDialog.show({
                templateUrl: 'inventory_partials/inventory_dialog_partial.html',
                controller: testcon,
               
            })

    }

    // delete function in cards start

    $scope.deleteproductview = function(arr,index){
        InvoiceService.removeArrayView(index, arr)
    }

    //delete function in cards stop 

     //TAB WATCHER START 
        $scope.changeTab = function(ind){
             switch (ind) {
                case 0:
                    $location.url("/home/receipt");
                    $rootScope.showaddInventory = true;
                  
                    break;
                case 1:
                    $location.url("/home/issue");
                    $rootScope.showaddInventory = false;
                   
                    break;
            }
        }; 
//TAB WATCHER END 

// sort function variable start
        $scope.testarr = [
                         {name:"Starred",id:"favouriteStarNo", src:"img/ic_grade_48px.svg",divider:true },
                         {name:"Date",id:"date", src:"img/ic_add_shopping_cart_48px.svg",divider:false},                         
                         {name:"Inventory No",id:"GRNno",  src:"img/ic_add_shopping_cart_48px.svg",divider:false},
                         {name:"Customer Names",id:"customerNames",  src:"img/ic_add_shopping_cart_48px.svg",divider:true},                         
                         {name:"Status 1",id:"Status",  src:"img/ic_add_shopping_cart_48px.svg",divider:false},
                         {name:"Status 2",id:"Status",  src:"img/ic_add_shopping_cart_48px.svg",divider:false},
                         {name:"Status 2",id:"Status",  src:"img/ic_add_shopping_cart_48px.svg",divider:false}
                         ]
 
 
         $rootScope.prodSearch = "";
         $scope.self = this;
         $scope.self.searchText = "";

        
        //sort function variable end 

        //Update Inventory records start
        $scope.inventoryupdate = function(updateForm) {
            var client = $objectstore.getClient("inventory12thdoor");
            console.log(updateForm);
            client.onComplete(function(data) {
                $mdDialog.show(
                    $mdDialog.alert()
                    .parent(angular.element(document.body))
                    .content('Inventory Successfully Updated')
                    .ariaLabel('')
                    .ok('OK')
                    .targetEvent(data)
                );
            });
            client.onError(function(data) {
                $mdDialog.show(
                    $mdDialog.alert()
                    .parent(angular.element(document.body))
                    .content('Error Updating Inventory')
                    .ariaLabel('')
                    .ok('OK')
                    .targetEvent(data)
                );
            });
            client.insert(updateForm, {
                KeyProperty: "inventory_code"
            });
        }
        $scope.InventoryDelete = function(deleteform) {

            var client = $objectstore.getClient("inventory12thdoor");
            client.onComplete(function(data) {
                $mdDialog.show(
                    $mdDialog.alert()
                    .parent(angular.element(document.body))
                    .content('Inventory Successfully Deleted')
                    .ariaLabel('')
                    .ok('OK')
                    .targetEvent(data)
                );
                $state.go($state.current, {}, {
                    reload: true
                });
            });
            client.onError(function(data) {
                $mdDialog.show(
                    $mdDialog.alert()
                    .parent(angular.element(document.body))
                    .content('Error Deleting Inventory')
                    .ariaLabel('')
                    .ok('OK')
                    .targetEvent(data)
                );
            });
            client.deleteSingle(deleteform.inventory_code, "inventory_code");
        }
        // Delete Inventory Records end 

        // Favourite star function strat
        $scope.favouriteFunction = function(obj) {
                var client = $objectstore.getClient("inventory12thdoor");
                client.onComplete(function(data) {

                    if (obj.inventoryFavourite) {
                        var toast = $mdToast.simple()
                            .content('Add To Favourite')
                            .action('OK')
                            .highlightAction(false)
                            .position("bottom right");
                        $mdToast.show(toast).then(function() {                           
                        });
                         obj.favouriteStarNo = 0;
                    } else if (!(obj.inventoryFavourite)) {

                        var toast = $mdToast.simple()
                            .content('Remove from Favourite')
                            .action('OK')
                            .highlightAction(false)
                            .position("bottom right");
                        $mdToast.show(toast).then(function() {
                         });
                         obj.favouriteStarNo = 1;
                    };
                });
                client.onError(function(data) {
                    var toast = $mdToast.simple()
                        .content('Error Occure while Adding to Favourite')
                        .action('OK')
                        .highlightAction(false)
                        .position("bottom right");
                    $mdToast.show(toast).then(function() {
                        //whatever
                    });
                });
                // if (obj.favouriteStarNo == 1 ) {
                //     obj.favouriteStarNo = 0;
                // }
                // else if (obj.favouriteStarNo == 0){
                //     obj.favouriteStarNo = 1;
                // }

                obj.inventoryFavourite = !obj.inventoryFavourite;
                client.insert(obj, {
                    KeyProperty: "inventory_code"
                });
            }
            // Favourite star function end
        $scope.addressfuncissueold = function(val) {

            if (val.AddressName == "Shipping Address") {
                val.AddressName = "Address";
            } else if (val.AddressName == "Address") {
                val.AddressName = "Shipping Address";
            };
        }

        $scope.addressfuncreceiptold = function(val) {
          
            if (val.AddressName == "Shipping Address") {
                val.AddressName = "Address";
            } else if (val.AddressName == "Address") {
                val.AddressName = "Shipping Address";
            };
        }        
        $scope.checkAbility = true;
        $scope.checkAbilityview = true;
        $scope.onChangeinventory = function(cbState) {

            if (cbState == true) {
                $scope.checkAbility = false;
                $scope.checkAbilityview = true;
            } else {
                $scope.checkAbility = true;
                $scope.checkAbilityview = false;
            }
        };
        //edit button ng-change function start
        $scope.onChangereceipt = function(cbState) {
            if (cbState == true) {
                $scope.checkAbilityview = false;
                $scope.checkAbility = true;
            } else {
                $scope.checkAbilityview = true;
                $scope.checkAbility = false;
            }
        };
        //edit button ng-button function end

        //start of fab button functions
  $scope.demo = {
            topDirections: ['left', 'up'],
            bottomDirections: ['down', 'right'],
            isOpen: false,
            availableModes: ['md-fling', 'md-scale'],
            selectedMode: 'md-fling',
            availableDirections: ['up', 'down', 'left', 'right'],
            selectedDirection: 'up'
        };

        $scope.addInventory = function() {
            

            $rootScope.inventoryType = "insert";
            $('#addinventory').animate({
                width: "100%",
                height: "100%",
                borderRadius: "0px",
                right: "0px",
                bottom: "0px",
                opacity: 0.25
            }, 400, function() {
                location.href = '#/Add_Inventory';
            });
        }


          $scope.addInventoryIssue = function() {
            

            $rootScope.inventoryType = "insert";
            $('#addinventory').animate({
                width: "100%",
                height: "100%",
                borderRadius: "0px",
                right: "0px",
                bottom: "0px",
                opacity: 0.25
            }, 400, function() {
                location.href = '#/Add_Inventory_Issue';
            });
        }
        //end of fab button functions

                $scope.Inventories = [];
                $scope.loadArray = {
                    val: []
                };

        //load all Inventory function start
        $scope.loadAllInventory = function() {
             // responsiveVoice.speak('wellcome to inventory');
            $scope.Inventories = [];
            $scope.arissues = [];
            $scope.arReceipt = [];
            var client = $objectstore.getClient("inventory12thdoor");
            client.onGetMany(function(data) {
                if (data) {
                    $scope.Inventories = data;
                    for (var d = 0, len = $scope.Inventories.length; d < len; d++) {
                        if ($scope.Inventories[d].inventoryClass == "Issue") {
                            $scope.arissues.push($scope.Inventories[d]);
                        } else if ($scope.Inventories[d].inventoryClass == "Receipt") {
                            $scope.arReceipt.push($scope.Inventories[d]);
                        }
                    }
                    $rootScope.inventoryType = "cards";
                }
            });
            client.onError(function(data) {
                $mdDialog.show(
                    $mdDialog.alert()
                    .parent(angular.element(document.body))
                    .title('This is embarracing')
                    .content('There was an error retreving the data.')
                    .ariaLabel('Alert Dialog Demo')
                    .ok('OK')
                    .targetEvent(data)
                );
            });
            client.getByFiltering("*");

            if ($state.current.name == 'home.receipt') {
                $rootScope.showaddInventory = true;
                $scope.selectedIndex = 0;
            }
            else if ($state.current.name == 'home.issue') {
                $rootScope.showaddInventory = false;
                $scope.selectedIndex = 1;
            };

        };
        //load all Inventory function end
      $scope.addproductview = function(arr) {
            $rootScope.tableContent = [];
            $rootScope.tableContent = arr;
            dialogsvc.addmethod();
        }
     // table ng click start

        $rootScope.tableContent = [];
        $scope.tableclick = function(arr,obj) {
            $rootScope.tableContent = [];
            $rootScope.tableContent = arr;
             dialogsvc.method(obj)
        }     
      //table ng click end
})
.factory('InvoiceService', function($rootScope) {
    $rootScope.testArray = {
        val: []
    };
    return {
        setArray: function(newVal) {
            $rootScope.testArray.val.push(newVal);
            return $rootScope.testArray;
        },
        setArrayview: function(val, arr) {
            arr.push(val);
            return arr;
        },
        removeArray: function(newVals) {
            $rootScope.testArray.val.splice(newVals, 1);
            return $rootScope.testArray;
        },
        removeArrayView: function(val, arr) {
            arr.splice(val, 1);
            return arr;
        }
    }
})
.service('dialogsvc', function($mdDialog){
    var  dialogsvc = {};
    dialogsvc.method = function(obj){
            $mdDialog.show({
                templateUrl: 'inventory_partials/inventory_dialog_partial.html',
                controller: EyeController,
                locals: {
                    item: obj
                }
            })
    }

    dialogsvc.addmethod = function(){
         $mdDialog.show({
                templateUrl: 'inventory_partials/inventory_dialog_partial.html',
                controller: DialogController
            })
    }
    function DialogController($scope,$objectstore, $mdDialog, InvoiceService, $rootScope,$state) {

         
       $scope.tenants = loadAll();
       $scope.selectedItem = null;
       $scope.searchText = null;
       $scope.querySearch = querySearch; 

        function querySearch(query) {
            $scope.enter = function(keyEvent) {
                if (keyEvent.which === 13) {
                    if ($scope.selectedItem === null) {
                        $scope.selectedItem = query;
                        console.log(results);
                    } else {
                        console.log($scope.selectedItem);
                    }
                }
            }
            //Custom Filter
            $scope.results = [];
            for (i = 0, len = $scope.productNames.length; i < len; ++i) {
                if ($scope.productNames[i].display.indexOf(query.toLowerCase()) != -1) {
                    $scope.results.push($scope.productNames[i]);
                }
            }

            return $scope.results;
        }
      
        $scope.productNames = [];

        function loadAll() {
            var client = $objectstore.getClient("12thproduct");
            client.onGetMany(function(data) {
                if (data) {
                     for (i = 0, len = data.length; i < len; ++i) {
                        $scope.productNames.push({
                            display: data[i].Productname,
                            value: data[i].Productname.toLowerCase()

                             });
                    }

                  }
            });
            client.onError(function(data) {
                $mdDialog.show(
                    $mdDialog.alert()
                    .parent(angular.element(document.body))
                    .title('Sorry')
                    .content('There is no products available')
                    .ariaLabel('Alert Dialog Demo')
                    .ok('OK')
                    .targetEvent(data)
                );
            });
            client.getByFiltering("*");
        }  
            $scope.inventory = {};
            $scope.dialogaddbutton = true;
            $scope.dialogUpdatebutton = false;
             if ($state.current.name == 'Add_Inventory' || $state.current.name == 'home.receipt'  ) {
                  $scope.plusdialog = true;
                }else if ($state.current.name == 'Add_Inventory_Issue' || $state.current.name == 'home.issue' ) {
                   $scope.plusdialog = false;
                }
 

            $scope.closeDialog = function(product) {

                $mdDialog.hide();
            }
            $scope.AddCus = function(product, qty, unit) {
               $scope.inventory.productname = product;
               
                if ($rootScope.inventoryType == "cards") {
                    if (product == null || qty == null || unit == null) {
                        console.log("fill all details")
                    } else {
                       InvoiceService.setArrayview($scope.inventory, $rootScope.tableContent);
                       $mdDialog.hide();
                    }
                } else if ($rootScope.inventoryType == "insert") {
                    if (product == null || qty == null || unit == null) {
                        console.log("fill all details")
                    } else {
                       InvoiceService.setArray($scope.inventory);
                       $mdDialog.hide();
                    }
                }
            }
        }
      function EyeController($scope, $mdDialog, InvoiceService, item, $rootScope,$state,$objectstore) {
            $scope.inventory = item;
            $scope.dialogaddbutton = false;
            $scope.dialogUpdatebutton = true;


       $scope.tenants = loadAll();
       $scope.selectedItem = item.productname;
       $scope.searchText = null;
       $scope.querySearch = querySearch; 

        function querySearch(query) {
            $scope.enter = function(keyEvent) {
                if (keyEvent.which === 13) {
                    if ($scope.selectedItem === null) {
                        $scope.selectedItem = query;
                        console.log(results);
                    } else {
                        console.log($scope.selectedItem);
                    }
                }
            }
            //Custom Filter
            $scope.results = [];
            for (i = 0, len = $scope.productNames.length; i < len; ++i) {
                if ($scope.productNames[i].display.indexOf(query.toLowerCase()) != -1) {
                    $scope.results.push($scope.productNames[i]);
                }
            }

            return $scope.results;
        }
      
        $scope.productNames = [];

        function loadAll() {
            var client = $objectstore.getClient("12thproduct");
            client.onGetMany(function(data) {
                if (data) {
                     for (i = 0, len = data.length; i < len; ++i) {
                        $scope.productNames.push({
                            display: data[i].Productname,
                            value: data[i].Productname.toLowerCase()

                             });
                    }

                  }
            });
            client.onError(function(data) {
                $mdDialog.show(
                    $mdDialog.alert()
                    .parent(angular.element(document.body))
                    .title('Sorry')
                    .content('There is no products available')
                    .ariaLabel('Alert Dialog Demo')
                    .ok('OK')
                    .targetEvent(data)
                );
            });
            client.getByFiltering("*");
        }  

             if ($state.current.name == 'Add_Inventory' || $state.current.name == 'home.receipt'  ) {
                  $scope.plusdialog = true;
                }else if ($state.current.name == 'Add_Inventory_Issue' || $state.current.name == 'home.issue' ) {
                   $scope.plusdialog = false;
                }
 
            $scope.closeDialog = function(product) {             
                    item.productname = product;
                    $mdDialog.hide();               
               
            }
            $scope.deletecus = function() {
                if ($rootScope.inventoryType == "cards") {
                    InvoiceService.removeArrayView(item, $rootScope.tableContent);
                    $mdDialog.hide();
                } else if ($rootScope.inventoryType == "insert") {
                    InvoiceService.removeArray(item);
                    $mdDialog.hide();
                }
            }
        }
    return dialogsvc;
})
