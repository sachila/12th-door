angular.module('mainApp', ['ngMaterial', 'directivelibrary', 'uiMicrokernel','ngAnimate','ui.router'])

    .config(function($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/settings/contact');
        $stateProvider
            .state('settings', {
            url: '/settings',
            templateUrl: 'contact_partial/settings.html',
            controller: 'AppCtrlGet'
            })
            .state('settings.contact', {
                url: '/contact',                
                templateUrl: 'contact_partial/contact_view.html',
                controller: 'AppCtrlGet'
            
            })
            .state('settings.supplier', {
                url: '/supplier',                
                templateUrl: 'contact_partial/supplier_view.html',
                controller: 'AppCtrlGetTimesheet'
            }) 
            .state('addcontact', {
                url: '/Add_Contact',                
                templateUrl: 'contact_partial/contact_add.html',
                controller: 'AppCtrlAdd'
            }) 
            .state('addsupplier', {
                url: '/Add_Supplier',                
                templateUrl: 'contact_partial/supplier_add.html',
                controller: 'AppCtrlAddTimesheet'
            }) 
    })

    .config(function($mdThemingProvider) {
        $mdThemingProvider.theme('datePickerTheme')
            .primaryPalette('teal');
    })

    //*****************ADD CONTROLLER********************************

    .controller('AppCtrlAdd', function($scope, $state, $objectstore, $location,
        $mdDialog, $window, $objectstore, $auth, $q,
        $http, $compile, $timeout, $mdToast) {
    
        $scope.contact = {};
		$scope.baddress = {};
		$scope.saddress = {};
		$scope.showShipping = $scope.showShipping;
		$scope.showBilling = !$scope.showBilling;
    
        $scope.submit = function() {
			var client = $objectstore.getClient("contact");
		client.onComplete(function(data) {
			$mdDialog.show(
					$mdDialog.alert()
					.parent(angular.element(document.body))
					.content('Customer Registed Successfully Saved.')
					.ariaLabel('Alert Dialog Demo')
					.ok('OK')
					.targetEvent(data)
				);
			});
			client.onError(function(data) {
				$mdDialog.show(
					$mdDialog.alert()
					.parent(angular.element(document.body))
					.content('There was an error saving the data.')
					.ariaLabel('Alert Dialog Demo')
					.ok('OK')
					.targetEvent(data)
				);
			});
			$scope.contact.favoritestar = false;
			$scope.contact.customerid = "-999";
			client.insert($scope.contact, {
				KeyProperty: "customerid"
			})
            //alert("hit");
		}
         
        
        $scope.addTimesheet = function() {
            $('#add')
                .animate({
                    width: "100%",
                    height: "100%",
                    borderRadius: "0px",
                    right: "0px",
                    bottom: "0px",
                    opacity: 0.25
                }, 400, function() {
                    location.href = '#/Add_Supplier';
                });
        }
        
		$scope.addressChange = function() {
			$scope.showShipping = !$scope.showShipping;
			$scope.showBilling = !$scope.showBilling;
		}
        
		$scope.demo = {
			topDirections: ['left', 'up'],
			bottomDirections: ['down', 'right'],
			isOpen: false,
			availableModes: ['md-fling', 'md-scale'],
			selectedMode: 'md-fling',
			availableDirections: ['up', 'down', 'left', 'right'],
			selectedDirection: 'up'
		};
    
		$scope.save = function() {
		    $timeout(function(){
           		 $('#mySignup')
               		 .click();
            	    })
		}
        
		$scope.viewpromotion = function() {
			$('#view').animate({
				width: "100%",
				height: "100%",
				borderRadius: "0px",
				right: "0px",
				bottom: "0px",
				opacity: 0.25
			}, 400, function() {});
		}
        
		$scope.addCustomer = function() {
			$('#add').animate({
				width: "100%",
				height: "100%",
				borderRadius: "0px",
				right: "0px",
				bottom: "0px",
				opacity: 0.25
			}, 400, function() {
				location.href = '#/Add_Contact';
			});
		}
        
		$scope.Customerview = function() {
			$('#view').animate({
				width: "100%",
				height: "100%",
				borderRadius: "0px",
				right: "0px",
				bottom: "0px",
				opacity: 0.25
			}, 400, function() {
				location.href = '#/home';
			});
		}
        
		$scope.savebtn = function() {
			$('#save').animate({
				width: "100%",
				height: "100%",
				borderRadius: "0px",
				right: "0px",
				bottom: "0px",
				opacity: 0.25
			}, 400, function() {
				$('#mySignup').click();
			});
		}
    }) //END OF AppCtrlAdd

    //***************RETRIVE CONTROLLER**********************

    .controller('AppCtrlGet', function($scope, $rootScope,$state, $objectstore, $location,
        $mdDialog, $window, $objectstore, $auth, $q,
        $http, $compile, $timeout, $mdToast) {
    
    
    
        // sort function variable start
 $scope.sortarr = [{name:"Starred",id:"Starred", src:"img/ic_grade_48px.svg", upstatus:false, downstatus:false },
                  {name:"Date",id:"date", src:"img/ic_add_shopping_cart_48px.svg", upstatus:true, downstatus: false},                         
                  {name:"Name",id:"name",  src:"img/ic_add_shopping_cart_48px.svg", upstatus:false, downstatus:false}]
 
        $rootScope.prodSearch = "date";
        $scope.self = this;
        $scope.indexno = 1;

        $scope.starfunc = function(item,index) {
            if (item.id === "Starred") {
                $scope.self.searchText = "true";
                console.log(JSON.stringify($scope.self))
            }
            else{

                if (item.upstatus == false && item.downstatus == false) {
                    item.upstatus = !item.upstatus;
                    $scope.sortarr[$scope.indexno].upstatus = false;
                    $scope.sortarr[$scope.indexno].downstatus = false;
                    $scope.indexno  = index;
                }
                else{
                 item.upstatus = !item.upstatus;
                 item.downstatus = !item.downstatus;             
                 }                
                               
                self.searchText = null;
                 
                if (item.upstatus) {
                     $rootScope.prodSearch = item.id;
                }
                if (item.downstatus) {
                    $rootScope.prodSearch = '-'+item.id;
                }
            }

            
        }
        //sort function variable end 
        
    
        $scope.projects = [];
        $scope.checkAbilityBtn = true;
        $scope.checkAbilityEditing = true;
        $scope.proSearch = "";
        
        if ($state.current.name == 'settings.project') {
                $scope.selectedIndex = 0;
            }
            else if ($state.current.name == 'settings.timesheet') {
                $scope.selectedIndex = 1;
         };
        $scope.loadAllprojects = function() {
            var client = $objectstore.getClient("project");
            client.onGetMany(function(data) {
                if (data) {
                    $scope.projects = data;
                }
            });
            client.onError(function(data) {
                $mdDialog.show($mdDialog.alert()
                    .parent(angular
                        .element(document.body))
                    .title(
                        'This is embarracing')
                    .content(
                        'There was an error retreving the data.'
                    )
                    .ariaLabel('Alert Dialog Demo')
                    .ok(
                        'OK')
                    .targetEvent(data));
            });
            client.getByFiltering("*");
        };
    
        $scope.updateProject = function(updatedform, pid) {
            var client = $objectstore.getClient("project");
            client.onComplete(function(data) {
                $mdDialog.show($mdDialog.alert()
                    .parent(angular
                        .element(document.body))
                    .content(
                        'Project details updated Successfully'
                    )
                    .ariaLabel('Alert Dialog Demo')
                    .ok(
                        'OK')
                    .targetEvent(data));
            });
            client.onError(function(data) {
                $mdDialog.show($mdDialog.alert()
                    .parent(angular
                        .element(document.body))
                    .title(
                        'This is embarracing')
                    .content(
                        'There was an error updating the project details.'
                    )
                    .ariaLabel('Alert Dialog Demo')
                    .ok(
                        'OK')
                    .targetEvent(data));
            });
            updatedform.projectid = pid;
            client.insert(updatedform, {
                KeyProperty: "projectid"
            });
        }
        
        $scope.deleteProject = function(deleteform, ev) {
            var confirm = $mdDialog.confirm()
                .parent(angular.element(
                    document.body))
                .title('')
                .content(
                    'Are You Sure You Want To Delete This Record?')
                .ok(
                    'Delete')
                .cancel('Cancel')
                .targetEvent(ev);
            $mdDialog.show(confirm)
                .then(function() {
                    var client = $objectstore.getClient("project");
                    client.onComplete(function(data) {
                        $mdDialog.show($mdDialog.alert()
                            .parent(
                                angular.element(
                                    document.body))
                            .content(
                                'Record Successfully Deleted'
                            )
                            .ariaLabel('')
                            .ok('OK')
                            .targetEvent(
                                data));
                        $state.go($state.current, {}, {
                            reload: true
                        });
                    });
                    client.onError(function(data) {
                        $mdDialog.show($mdDialog.alert()
                            .parent(
                                angular.element(
                                    document.body))
                            .content(
                                'Error Deleting Record'
                            )
                            .ariaLabel('')
                            .ok('OK')
                            .targetEvent(
                                data));
                    });
                    client.deleteSingle(deleteform.projectid,
                        "projectid");
                }, function() {
                    $mdDialog.hide();
                });
        }
        
        $scope.favouriteFunction = function(obj) {
            var client = $objectstore.getClient("project");
            client.onComplete(function(data) {
                if (obj.favoritestar) {
                    var toast = $mdToast.simple()
                        .content(
                            'Add To Favourite')
                        .action('OK')
                        .highlightAction(
                            false)
                        .position("bottom right");
                    $mdToast.show(toast)
                        .then(function() {});
                } else if (!(obj.favoritestar)) {
                    var toast = $mdToast.simple()
                        .content(
                            'Remove from Favourite')
                        .action(
                            'OK')
                        .highlightAction(false)
                        .position(
                            "bottom right");
                    $mdToast.show(toast)
                        .then(function() {});
                };
            });
            client.onError(function(data) {
                $mdDialog.show($mdDialog.alert()
                    .parent(angular
                        .element(document.body))
                    .content(
                        'Error Occure while Adding to Favourite'
                    )
                    .ariaLabel('')
                    .ok('OK')
                    .targetEvent(
                        data));
            });
            obj.favoritestar = !obj.favoritestar;
            client.insert(obj, {
                KeyProperty: "projectid"
            });
        }
        
        $scope.sortFunciton = function(name) {
            $scope.projectSearch = name;
            self.searchText = null;
        }
        
        $scope.testfunc = function() {
            self.searchText = "true"
        }
        
        $scope.addStaffupdate = function(add) {
            add.push({
                sno: add.length + 1,
                staffname: "",
                shr: "",
                removebtnDisable:true
            })
        }
        
        $scope.removeStaffupdate = function(index, proj) {
            $scope.projects[index].staffs.splice(proj, 1);
        }
        
        $scope.addTaskupdate = function(add) {
            add.push({
                tno: add.length + 1,
                taskName: "",
                thr: "",
                removebtnDisable: true
            })
        }
        
        $scope.removeTaskupdate = function(index, proj) {
            $scope.projects[index].tasks.splice(proj, 1);
        }
        
        $scope.onChangeEditing = function(cbState, state) {
            if (cbState == true) {
                $scope.checkAbilityEditing = false;
                $scope.checkAbilityBtn = false;
            } else {
                $scope.checkAbilityEditing = true;
                $scope.checkAbilityBtn = true;
            }
            for (var i = state.length - 1; i >= 0; i--) {
                state[i].removebtnDisable = true;
            };
        }
        
        $scope.chekAblityRemovebtn = function(state) {
            for (var i = state.length - 1; i >= 0; i--) {
                state[i].removebtnDisable = true;
            };
        }
        
        $rootScope.showsort = true;
        $rootScope.showaddProject = true;
        $rootScope.showsort = true;
        $scope.changeTab = function(ind){
             switch (ind) {
                case 0:
                    $location.url("/settings/contact");
                    
                    $rootScope.showsort = true;
                    break;
                case 1:
                    $location.url("/settings/supplier");
                   
                    $rootScope.showsort = false;
					$rootScope.showaddProject = false;
                     
                    break;
            }
        };
        
        $scope.demo = {
            topDirections: ['left', 'up'],
            bottomDirections: ['down', 'right'],
            isOpen: false,
            availableModes: ['md-fling', 'md-scale'],
            selectedMode: 'md-fling',
            availableDirections: ['up', 'down', 'left', 'right'],
            selectedDirection: 'up'
        };
    
        $scope.addCustomer = function() {
            $('#add')
                .animate({
                    width: "100%",
                    height: "100%",
                    borderRadius: "0px",
                    right: "0px",
                    bottom: "0px",
                    opacity: 0.25
                }, 400, function() {
                    location.href = '#/Add_Project';
                });
        }
        
         $scope.addTimesheet = function() {
            $('#add')
                .animate({
                    width: "100%",
                    height: "100%",
                    borderRadius: "0px",
                    right: "0px",
                    bottom: "0px",
                    opacity: 0.25
                }, 400, function() {
                    location.href = '#/Add_Supplier';
                });
        }
        
        
        $scope.Customerview = function() {
            $('#view')
                .animate({
                    width: "100%",
                    height: "100%",
                    borderRadius: "0px",
                    right: "0px",
                    bottom: "0px",
                    opacity: 0.25
                }, 400, function() {
                    location.href = '#/home';
                });
        }
        
        $scope.savebtn = function() {
            $('#save')
                .animate({
                    width: "100%",
                    height: "100%",
                    borderRadius: "0px",
                    right: "0px",
                    bottom: "0px",
                    opacity: 0.25
                }, 400, function() {
                    $('#mySignup')
                        .click();
                });
        }
        
        $scope.save = function() {
             $timeout(function(){
            $('#mySignup')
                .click();
             })
        }
        
        $scope.viewpromotion = function() {
            $('#view')
                .animate({
                    width: "100%",
                    height: "100%",
                    borderRadius: "0px",
                    right: "0px",
                    bottom: "0px",
                    opacity: 0.25
                }, 400, function() {});
        }
        ////////////////////////////////////////
        $scope.contacts = [];
		$scope.baddress = {};
		$scope.saddress = {};
		$scope.showShipping = $scope.showShipping;
		$scope.showBilling = !$scope.showBilling;
		$scope.checkAbilityBtn = true;
		$scope.checkAbilityEditing = true;
		$scope.prodSearch = "";
		$scope.sortName = "CustomerFname";
		$scope.sortEmail = "Email";
		$scope.sortMobile = "Mobile";
		$scope.sortCity = "city";
		$scope.sortCountry = "country";
        $scope.sortNameAsc = "customerFname";
        $scope.sortNameDes = "-customerFname";
        $scope.contactNameupArrow = false;
        $scope.contactNamedownArrow = false;
        $scope.sortEmailAsc = "Email";
        $scope.sortEmailDes = "-Email";
        $scope.contactEmailupArrow = false;
        $scope.contactEmaildownArrow = false;

    
		$scope.loadAllcontact = function() {
			$scope.projects = [];
			var client = $objectstore.getClient("contact");
			client.onGetMany(function(data) {
				if (data) {
					$scope.contacts = data;
				}
			});
			client.onError(function(data) {
				$mdDialog.show(
					$mdDialog.alert()
					.parent(angular.element(document.body))
					.title('This is embarracing')
					.content('There was an error retreving the data.')
					.ariaLabel('Alert Dialog Demo')
					.ok('OK')
					.targetEvent(data)
				);
			});
			client.getByFiltering("*");
		};
    
		$scope.updateCustomer = function(updatedform, cid) {
			var client = $objectstore.getClient("contact");
			client.onComplete(function(data) {
				$mdDialog.show(
					$mdDialog.alert()
					.parent(angular.element(document.body))
					.content('Customer details updated Successfully')
					.ariaLabel('Alert Dialog Demo')
					.ok('OK')
					.targetEvent(data)
				);
			});
			client.onError(function(data) {
				$mdDialog.show(
					$mdDialog.alert()
					.parent(angular.element(document.body))
					.title('This is embarracing')
					.content('There was an error updating the customer details.')
					.ariaLabel('Alert Dialog Demo')
					.ok('OK')
					.targetEvent(data)
				);
			});
			updatedform.customerid = cid;
			client.insert(updatedform, {
				KeyProperty: "customerid"
			});
		}
        
		$scope.deleteContact = function(deleteform, ev) {
			var confirm = $mdDialog.confirm()
				.parent(angular.element(document.body))
				.title('')
				.content('Are You Sure You Want To Delete This Record?')
				.ok('Delete')
				.cancel('Cancel')
				.targetEvent(ev);
			$mdDialog.show(confirm).then(function() {
					var client = $objectstore.getClient("contact");
					client.onComplete(function(data) {
						$mdDialog.show(
							$mdDialog.alert()
							.parent(angular.element(document.body))
							.content('Record Successfully Deleted')
							.ariaLabel('')
							.ok('OK')
							.targetEvent(data)
						);
						$state.go($state.current, {}, {
							reload: true
						});
					});
					client.onError(function(data) {
						$mdDialog.show(
							$mdDialog.alert()
							.parent(angular.element(document.body))
							.content('Error Deleting Record')
							.ariaLabel('')
							.ok('OK')
							.targetEvent(data)
						);
					});
					client.deleteSingle(deleteform.customerid, "customerid");
				},
				function() {
					$mdDialog.hide();
				});
		}
        
		$scope.favouriteFunction = function(obj) {
			var client = $objectstore.getClient("contact");
			client.onComplete(function(data) {
				if (obj.favoritestar) {
					var toast = $mdToast.simple()
						.content('Add To Favourite')
						.action('OK')
						.highlightAction(false)
						.position("bottom right");
					$mdToast.show(toast).then(function() {});
				} else if (!(obj.favoritestar)) {
					var toast = $mdToast.simple()
						.content('Remove from Favourite')
						.action('OK')
						.highlightAction(false)
						.position("bottom right");
					$mdToast.show(toast).then(function() {});
				};
			});
			client.onError(function(data) {
				$mdDialog.show(
					$mdDialog.alert()
					.parent(angular.element(document.body))
					.content('Error Occure while Adding to Favourite')
					.ariaLabel('')
					.ok('OK')
					.targetEvent(data)
				);
			});
			obj.favoritestar = !obj.favoritestar;
			client.insert(obj, {
				KeyProperty: "customerid"
			});
		}
        
		$scope.addressChange = function() {
			$scope.showShipping = !$scope.showShipping;
			$scope.showBilling = !$scope.showBilling;
		}
        
		$scope.sortAll = function(name) {
			$scope.ContactSearch = name;
            if(name == $scope.sortName){
                  self.searchText = null; 

                  $scope.contactNamedownArrow = true; 
                  $scope.contactNameupArrow = false;

           }
           else if(name == $scope.sortEmail){
               self.searchText = null;
               
               $scope.contactEmaildownArrow=true;
               $scope.contactEmailupArrow=false;
               
               $scope.contactNamedownArrow=false;
               $scope.contactNameupArrow=false;
           }
			
		}
        
        $scope.sortFunction=function(name){
            $scope.ContactSearch = name;
            if (name == "-customerFname") {
                $scope.contactNameupArrow = true;
                $scope.contactNamedownArrow = false;
            }
            else if (name == "customerFname") {
                $scope.contactNameupArrow = false;
                $scope.contactNamedownArrow = true;
            }   
        }
        
        $scope.sortEmail = function(name){
    
                       $scope.contactSearch = name;
                      $rootScope.searchText1 = null;

                      if (name == "-Email") {
                      $scope.contactEmailupArrow = true;
                      $scope.contactEmaildownArrow = false;
                  }
                  else if (name == "Email") {
                       $scope.contactEmailupArrow = false;
                      $scope.contactEmaildownArrow = true;

                     };
                  
                }
        
		$scope.testfunc = function() {
			self.searchText = "true"
		}
        
		$scope.onChangeEditing = function(cbState) {
			if (cbState == true) {
				$scope.checkAbilityEditing = false;
				$scope.checkAbilityBtn = false;
			} else {
				$scope.checkAbilityEditing = true;
				$scope.checkAbilityBtn = true;
			}
		};
    
		$scope.demo = {
			topDirections: ['left', 'up'],
			bottomDirections: ['down', 'right'],
			isOpen: false,
			availableModes: ['md-fling', 'md-scale'],
			selectedMode: 'md-fling',
			availableDirections: ['up', 'down', 'left', 'right'],
			selectedDirection: 'up'
		};
    
		$scope.viewpromotion = function() {
			$('#view').animate({
				width: "100%",
				height: "100%",
				borderRadius: "0px",
				right: "0px",
				bottom: "0px",
				opacity: 0.25
			}, 400, function() {});
		}
        
		$scope.addCustomer = function() {
			$('#add').animate({
				width: "100%",
				height: "100%",
				borderRadius: "0px",
				right: "0px",
				bottom: "0px",
				opacity: 0.25
			}, 400, function() {
				location.href = '#/Add_Contact';
			});
		}
        
		$scope.Customerview = function() {
			$('#view').animate({
				width: "100%",
				height: "100%",
				borderRadius: "0px",
				right: "0px",
				bottom: "0px",
				opacity: 0.25
			}, 400, function() {
				location.href = '#/home';
			});
		}
        
		$scope.savebtn = function() {
			$('#save').animate({
				width: "100%",
				height: "100%",
				borderRadius: "0px",
				right: "0px",
				bottom: "0px",
				opacity: 0.25
			}, 400, function() {
				$('#mySignup').click();
			});
		}
    }) //End of ApCtrlGet

.controller('AppCtrlAddTimesheet', function($scope, $state, $objectstore, $location,
        $mdDialog, $window, $objectstore, $auth, $q,
        $http, $compile, $timeout, $mdToast,$rootScope) {
    
        $scope.project = {};   
        $scope.customerNames = [];
        $scope.contacts=[];
        $rootScope.results=[];
        
        //Autocomplete stuff
        $rootScope.self = this;
        $rootScope.self.tenants   = loadAll();
        $rootScope.selectedItem1  = null;
        $rootScope.self.searchText    = null;
        $rootScope.self.querySearch   = querySearch;

        function querySearch (query) {
            $scope.enter = function(keyEvent) {
                if (keyEvent.which === 13)
                { 
                    if($rootScope.selectedItem1 === null)
                    {
                        $rootScope.selectedItem1 = query;  
                        console.log($rootScope.results);
                    }else{
                        console.log($rootScope.selectedItem1);
                    }
                }
            }
      //Custom Filter
      for (i = 0, len = $scope.customerNames.length; i<len; ++i){
        if($scope.customerNames[i].display.indexOf(query) !=-1)
        {
          $rootScope.results.push($scope.customerNames[i]); 
        } 
      }
        
     console.log($rootScope.results);
      return $rootScope.results;
    }
     
    function loadAll() {
         var client = $objectstore.getClient("contact");
     client.onGetMany(function(data){
      if (data){ 
      // $scope.contact =data;
        for (i = 0, len = data.length; i<len; ++i){
          $scope.customerNames.push ({display: data[i].CustomerFname.toLowerCase(), value : data[i]});
        } 
          console.log($scope.customerNames);

      }
     }); 
     client.onError(function(data){
      $mdDialog.show(
      );
     });
     client.getByFiltering("*");
     
    }
    
        $scope.submit = function() {
            var client = $objectstore.getClient("supplier");
            client.onComplete(function(data) {
                $mdDialog.show($mdDialog.alert()
                    .parent(angular
                        .element(document.body))
                    .content(
                        'Timesheet Added Successfully.')
                    .ariaLabel(
                        'Alert Dialog Demo')
                    .ok('OK')
                    .targetEvent(
                        data));
            });
            client.onError(function(data) {
                $mdDialog.show($mdDialog.alert()
                    .parent(angular
                        .element(document.body))
                    .content(
                        'There was an error saving the data.'
                    )
                    .ariaLabel('Alert Dialog Demo')
                    .ok(
                        'OK')
                    .targetEvent(data));
            });
            $scope.supplier.favoritestar = false;
            $scope.supplier.supplierid = "-999";
            client.insert($scope.supplier, {
                KeyProperty: "supplierid"
            })
        }
           
       
       
        $scope.demo = {
            topDirections: ['left', 'up'],
            bottomDirections: ['down', 'right'],
            isOpen: false,
            availableModes: ['md-fling', 'md-scale'],
            selectedMode: 'md-fling',
            availableDirections: ['up', 'down', 'left', 'right'],
            selectedDirection: 'up'
        };
    
        ///////////////////////////////////////////////////////////////////////
        
        $scope.contact = {};
		$scope.baddress = {};
		$scope.saddress = {};
		$scope.showShipping = $scope.showShipping;
		$scope.showBilling = !$scope.showBilling;
    
		$scope.submit = function() {
			var client = $objectstore.getClient("supplier");
			client.onComplete(function(data) {
				$mdDialog.show(
					$mdDialog.alert()
					.parent(angular.element(document.body))
					.content('Supplier Registed Successfully Saved.')
					.ariaLabel('Alert Dialog Demo')
					.ok('OK')
					.targetEvent(data)
				);
			});
			client.onError(function(data) {
				$mdDialog.show(
					$mdDialog.alert()
					.parent(angular.element(document.body))
					.content('There was an error saving the data.')
					.ariaLabel('Alert Dialog Demo')
					.ok('OK')
					.targetEvent(data)
				);
			});
			$scope.supplier.favoritestar = false;
			$scope.supplier.supplierid = "-999";
			client.insert($scope.supplier, {
				KeyProperty: "supplierid"
			})
		}
        
		$scope.addressChange = function() {
			$scope.showShipping = !$scope.showShipping;
			$scope.showBilling = !$scope.showBilling;
		}
        
		$scope.demo = {
			topDirections: ['left', 'up'],
			bottomDirections: ['down', 'right'],
			isOpen: false,
			availableModes: ['md-fling', 'md-scale'],
			selectedMode: 'md-fling',
			availableDirections: ['up', 'down', 'left', 'right'],
			selectedDirection: 'up'
		};
    
		$scope.save = function() {
		    $timeout(function(){
            	   $('#mySignup')
                  .click();
                })
		}
        
		$scope.viewpromotion = function() {
			$('#view').animate({
				width: "100%",
				height: "100%",
				borderRadius: "0px",
				right: "0px",
				bottom: "0px",
				opacity: 0.25
			}, 400, function() {});
		}
        
		$scope.addCustomer = function() {
			$('#add').animate({
				width: "100%",
				height: "100%",
				borderRadius: "0px",
				right: "0px",
				bottom: "0px",
				opacity: 0.25
			}, 400, function() {
				location.href = '#/Add_Supplier';
			});
		}
        
		$scope.Customerview = function() {
			$('#view').animate({
				width: "100%",
				height: "100%",
				borderRadius: "0px",
				right: "0px",
				bottom: "0px",
				opacity: 0.25
			}, 400, function() {
				location.href = '#/home';
			});
		}
        
		$scope.savebtn = function() {
			$('#save').animate({
				width: "100%",
				height: "100%",
				borderRadius: "0px",
				right: "0px",
				bottom: "0px",
				opacity: 0.25
			}, 400, function() {
				$('#mySignup').click();
			});
		}
    }) //END OF AppCtrlAdd

 .controller('AppCtrlGetTimesheet', function($scope, $rootScope ,$state, $objectstore, $location,
        $mdDialog, $window, $objectstore, $auth, $q,
        $http, $compile, $timeout, $mdToast) {
    
    
        $rootScope.prodSearch = "date";
        $scope.self = this;
        $scope.indexnosort = 1;
    
  
    
    $scope.sortarrtimesheet = [{name:"Starred",id:"Starred", src:"img/ic_grade_48px.svg", upstatus:false, downstatus:false },
                  {name:"Date",id:"date", src:"img/ic_add_shopping_cart_48px.svg", upstatus:true, downstatus: false}, 
                 {name:"Time",id:"hours", src:"img/ic_add_shopping_cart_48px.svg", upstatus:false, downstatus: false}, 
                  {name:" Task Name",id:"task",  src:"img/ic_add_shopping_cart_48px.svg", upstatus:false, downstatus:false}]

        $scope.starfunc = function(item,index) {
            if (item.id === "Starred") {
                $scope.self.searchText = "true";
                console.log(JSON.stringify($scope.self))
            }
            else{

                if (item.upstatus == false && item.downstatus == false) {
                    item.upstatus = !item.upstatus;
                    $scope.sortarrtimesheet[$scope.indexnosort].upstatus = false;
                    $scope.sortarrtimesheet[$scope.indexnosort].downstatus = false;
                    $scope.indexnosort  = index;
                }
                else{
                 item.upstatus = !item.upstatus;
                 item.downstatus = !item.downstatus;             
                 }                
                               
                self.searchText = null;
                 
                if (item.upstatus) {
                     $rootScope.prodSearch = item.id;
                }
                if (item.downstatus) {
                    $rootScope.prodSearch = '-'+item.id;
                }
            }

            
        }
        //sort function variable end 
    
        $rootScope.showaddProject = false;
        $scope.suppliers = [];
        $scope.checkAbilityBtn = true;
        $scope.checkAbilityEditing = true;
        $scope.proSearch = "";
        

        
        $scope.sortFunciton = function(name) {
            $scope.projectSearch = name;
            self.searchText = null;
        }
        
        $scope.testfunc = function() {
            self.searchText = "true"
        }
        
        $scope.showProjectMenu= true;
        $rootScope.showsort = false;
        //////////////////////////////////////////////
        
        $scope.suppliers = [];
		$scope.baddress = {};
		$scope.saddress = {};
		$scope.showShipping = $scope.showShipping;
		$scope.showBilling = !$scope.showBilling;
		$scope.checkAbilityBtn = true;
		$scope.checkAbilityEditing = true;
		$scope.prodSearch = "";
		$scope.sortName = "CustomerFname";
		$scope.sortEmail = "Email";
		$scope.sortMobile = "Mobile";
		$scope.sortCity = "city";
		$scope.sortCountry = "country";
        $scope.sortNameAsc = "customerFname";
        $scope.sortNameDes = "-customerFname";
        $scope.contactNameupArrow = false;
        $scope.contactNamedownArrow = false;
        $scope.sortEmailAsc = "Email";
        $scope.sortEmailDes = "-Email";
        $scope.contactEmailupArrow = false;
        $scope.contactEmaildownArrow = false;

    
		$scope.loadAllsupplier = function() {
			$scope.projects = [];
			var client = $objectstore.getClient("supplier");
			client.onGetMany(function(data) {
				if (data) {
					$scope.suppliers = data;
				}
			});
			client.onError(function(data) {
				$mdDialog.show(
					$mdDialog.alert()
					.parent(angular.element(document.body))
					.title('This is embarracing')
					.content('There was an error retreving the data.')
					.ariaLabel('Alert Dialog Demo')
					.ok('OK')
					.targetEvent(data)
				);
			});
			client.getByFiltering("*");
		};
    
		$scope.updateCustomer = function(updatedform, cid) {
			var client = $objectstore.getClient("contact");
			client.onComplete(function(data) {
				$mdDialog.show(
					$mdDialog.alert()
					.parent(angular.element(document.body))
					.content('Customer details updated Successfully')
					.ariaLabel('Alert Dialog Demo')
					.ok('OK')
					.targetEvent(data)
				);
			});
			client.onError(function(data) {
				$mdDialog.show(
					$mdDialog.alert()
					.parent(angular.element(document.body))
					.title('This is embarracing')
					.content('There was an error updating the customer details.')
					.ariaLabel('Alert Dialog Demo')
					.ok('OK')
					.targetEvent(data)
				);
			});
			updatedform.customerid = cid;
			client.insert(updatedform, {
				KeyProperty: "customerid"
			});
		}
        
		$scope.deleteContact = function(deleteform, ev) {
			var confirm = $mdDialog.confirm()
				.parent(angular.element(document.body))
				.title('')
				.content('Are You Sure You Want To Delete This Record?')
				.ok('Delete')
				.cancel('Cancel')
				.targetEvent(ev);
			$mdDialog.show(confirm).then(function() {
					var client = $objectstore.getClient("contact");
					client.onComplete(function(data) {
						$mdDialog.show(
							$mdDialog.alert()
							.parent(angular.element(document.body))
							.content('Record Successfully Deleted')
							.ariaLabel('')
							.ok('OK')
							.targetEvent(data)
						);
						$state.go($state.current, {}, {
							reload: true
						});
					});
					client.onError(function(data) {
						$mdDialog.show(
							$mdDialog.alert()
							.parent(angular.element(document.body))
							.content('Error Deleting Record')
							.ariaLabel('')
							.ok('OK')
							.targetEvent(data)
						);
					});
					client.deleteSingle(deleteform.customerid, "customerid");
				},
				function() {
					$mdDialog.hide();
				});
		}
        
		$scope.favouriteFunction = function(obj) {
			var client = $objectstore.getClient("contact");
			client.onComplete(function(data) {
				if (obj.favoritestar) {
					var toast = $mdToast.simple()
						.content('Add To Favourite')
						.action('OK')
						.highlightAction(false)
						.position("bottom right");
					$mdToast.show(toast).then(function() {});
				} else if (!(obj.favoritestar)) {
					var toast = $mdToast.simple()
						.content('Remove from Favourite')
						.action('OK')
						.highlightAction(false)
						.position("bottom right");
					$mdToast.show(toast).then(function() {});
				};
			});
			client.onError(function(data) {
				$mdDialog.show(
					$mdDialog.alert()
					.parent(angular.element(document.body))
					.content('Error Occure while Adding to Favourite')
					.ariaLabel('')
					.ok('OK')
					.targetEvent(data)
				);
			});
			obj.favoritestar = !obj.favoritestar;
			client.insert(obj, {
				KeyProperty: "customerid"
			});
		}
        
		$scope.addressChange = function() {
			$scope.showShipping = !$scope.showShipping;
			$scope.showBilling = !$scope.showBilling;
		}
        
		$scope.sortAll = function(name) {
			$scope.ContactSearch = name;
            if(name == $scope.sortName){
                  self.searchText = null; 

                  $scope.contactNamedownArrow = true; 
                  $scope.contactNameupArrow = false;

           }
           else if(name == $scope.sortEmail){
               self.searchText = null;
               
               $scope.contactEmaildownArrow=true;
               $scope.contactEmailupArrow=false;
               
               $scope.contactNamedownArrow=false;
               $scope.contactNameupArrow=false;
           }
			
		}
        
        $scope.sortFunction=function(name){
            $scope.ContactSearch = name;
            if (name == "-customerFname") {
                $scope.contactNameupArrow = true;
                $scope.contactNamedownArrow = false;
            }
            else if (name == "customerFname") {
                $scope.contactNameupArrow = false;
                $scope.contactNamedownArrow = true;
            }   
        }
        
        $scope.sortEmail = function(name){
    
                       $scope.contactSearch = name;
                      $rootScope.searchText1 = null;

                      if (name == "-Email") {
                      $scope.contactEmailupArrow = true;
                      $scope.contactEmaildownArrow = false;
                  }
                  else if (name == "Email") {
                       $scope.contactEmailupArrow = false;
                      $scope.contactEmaildownArrow = true;

                     };
                  
                }
        
		$scope.testfunc = function() {
			self.searchText = "true"
		}
        
		$scope.onChangeEditing = function(cbState) {
			if (cbState == true) {
				$scope.checkAbilityEditing = false;
				$scope.checkAbilityBtn = false;
			} else {
				$scope.checkAbilityEditing = true;
				$scope.checkAbilityBtn = true;
			}
		};
    
		$scope.demo = {
			topDirections: ['left', 'up'],
			bottomDirections: ['down', 'right'],
			isOpen: false,
			availableModes: ['md-fling', 'md-scale'],
			selectedMode: 'md-fling',
			availableDirections: ['up', 'down', 'left', 'right'],
			selectedDirection: 'up'
		};
    
		$scope.viewpromotion = function() {
			$('#view').animate({
				width: "100%",
				height: "100%",
				borderRadius: "0px",
				right: "0px",
				bottom: "0px",
				opacity: 0.25
			}, 400, function() {});
		}
        
		$scope.addCustomer = function() {
			$('#add').animate({
				width: "100%",
				height: "100%",
				borderRadius: "0px",
				right: "0px",
				bottom: "0px",
				opacity: 0.25
			}, 400, function() {
				location.href = '#/Add_Supplier';
			});
		}
        
		$scope.Customerview = function() {
			$('#view').animate({
				width: "100%",
				height: "100%",
				borderRadius: "0px",
				right: "0px",
				bottom: "0px",
				opacity: 0.25
			}, 400, function() {
				location.href = '#/home';
			});
		}
        
		$scope.savebtn = function() {
			$('#save').animate({
				width: "100%",
				height: "100%",
				borderRadius: "0px",
				right: "0px",
				bottom: "0px",
				opacity: 0.25
			}, 400, function() {
				$('#mySignup').click();
			});
		}
        
    }) //End of ApCtrlGet