angular
 .module('mainApp', ['ngMaterial','directivelibrary','12thdirective','uiMicrokernel','paymentGateway','ui.router'])
 .config(function($stateProvider, $urlRouterProvider) {    
  $urlRouterProvider.otherwise('/home');    
  $stateProvider       
       	  .state('home', {
            url: '/home',
           templateUrl: 'expenses_partial/expenses_card_partial.html',
           controller:'viewctrl'
        }).state('Add_Expenses', {
            url: '/Add_Expenses',
           templateUrl: 'expenses_partial/expense_add_partial.html',
           controller:'AppCtrl'
        })
   })
.config(function($mdThemingProvider) {
	  $mdThemingProvider.theme('datePickerTheme').primaryPalette('teal');
	})
 
.controller('AppCtrl', function ($scope,$mdToast, $uploader, $rootScope, UploaderService,$objectstore, $mdDialog, mfbDefaultValues, $window, $objectstore, $auth, $q, $http, $compile, $timeout) {

//auto complete for customer and projects start
			var self = this;
				// list of `state` value/display objects
				self.tenants       = loadAll();
				self.selectedItem  = null;
				self.searchText    = null;
				self.querySearch   = querySearch;	

				$scope.calanderdisable = true;
				$scope.calanderfun = function(obj){
					if (obj.Status == "Paid") {
						$scope.calanderdisable = true;
						// $scope.Duedate = null;

					}else if (obj.Status == "Unpaid") {
						$scope.calanderdisable = false;
					};
				}	  

				 function querySearch (query) {
				   $scope.enter = function(keyEvent) {
		                if (keyEvent.which === 13) {
		                    if (self.selectedItem === null) {
		                        self.selectedItem = query;
		                        console.log(results);
		                    } else {
		                        console.log(self.selectedItem);
		                    }
		                }
		            }
				 		var results = [];
			            for (i = 0, len = $scope.fullarr.length; i < len; ++i) {
			                if ($scope.fullarr[i].value.indexOf(query.toLowerCase()) != -1) {
			                    results.push($scope.fullarr[i]);
			                }
			            }

			            return results;				 
				}

				$scope.projectarr = [];
				$scope.customerarr = [];
				$scope.fullarr = [];

				function loadAll(){

					var client = $objectstore.getClient("project");
				client.onGetMany(function(data){
					if (data){
						$scope.projectarr = [];
						for (var i = data.length - 1; i >= 0; i--) {
							$scope.projectarr.push({
								display: data[i].name,
								value: data[i].name.toLowerCase(),
								image:"img/ic_shopping_cart_24px.svg"
							});
						};
						$scope.loadcontactFunc();						
					}
				});	
				client.onError(function(data){
					$mdDialog.show(
						$mdDialog.alert()
						.parent(angular.element(document.body))
						.title('This is embarracing')
						.content('There was an error retreving the Project Data.')
						.ariaLabel('Alert Dialog Demo')
						.ok('OK')
						.targetEvent(data)
						);
				});
				client.getByFiltering("*");
				}

		$scope.loadcontactFunc = function(){

					var client = $objectstore.getClient("contact");
				client.onGetMany(function(data){
					if (data){
						$scope.customerarr = [];
						for (var i = data.length - 1; i >= 0; i--) {
							$scope.customerarr.push({
								display: data[i].CustomerFname + ' ' + data[i].CustomerLname,
								value: data[i].CustomerFname.toLowerCase() + ' ' + data[i].CustomerLname.toLowerCase(),
								image:"img/ic_face_24px.svg"							
							});
						};			 
						 $scope.fullarr =$scope.customerarr.concat($scope.projectarr);
									
					}
				});	
				client.onError(function(data){
					$mdDialog.show(
						$mdDialog.alert()
						.parent(angular.element(document.body))
						.title('This is embarracing')
						.content('There was an error retreving the Contact Data.')
						.ariaLabel('Alert Dialog Demo')
						.ok('OK')
						.targetEvent(data)
						);
				});
				client.getByFiltering("*");

		}


//auto complete for customer and projects end 

 //save functon start
 	$scope.submit=function(){
 			$scope.imagearray = [];
			$scope.imagearray = UploaderService.loadArray();
			if ($scope.imagearray.length > 0) {
				for	(indexx = 0; indexx < $scope.imagearray.length; indexx++) {
					$uploader.upload("45.55.83.253","expenseimagesNew", $scope.imagearray[indexx]);
					$uploader.onSuccess(function(e,data){
					});
					$uploader.onError(function(e,data){							
						var toast = $mdToast.simple()
								  .content('There was an error, please upload!')
								  .action('OK')
								  .highlightAction(false)
								  .position("bottom right");
							$mdToast.show(toast).then(function() {
								//whatever
							});
					});							
					}
			};

			var client = $objectstore.getClient("expense12th");
			client.onComplete(function(data){		 
		    $mdDialog.show(
							$mdDialog.alert()
						   .parent(angular.element(document.body))
						   //.title('This is embarracing')
						   .content('Expense Successfully Saved.')
						   .ariaLabel('Alert Dialog Demo')
						   .ok('OK')
						   .targetEvent(data)
						);
			});        
			client.onError(function(data){
			$mdDialog.show(
							$mdDialog.alert()
						   .parent(angular.element(document.body))
						   .content('There was an error saving the data.')
						   .ariaLabel('Alert Dialog Demo')
						   .ok('OK')
						   .targetEvent(data)
						);				
			});

			$scope.expense.totalUSD = $scope.totalUSD;
			$scope.expense.disableForm = false;
		 	$scope.expense.favouriteStar = false;
		 	$scope.expense.favouriteStarNo = 1;          
            $scope.expense.expense_code = "-999";
			$scope.expense.Duedate = $scope.Duedate;
			$scope.expense.UploadImages = {val: []};
			$scope.expense.UploadImages.val = UploaderService.loadBasicArray();	
		    $scope.expense.Assigncustomer = self.selectedItem.display;		
			client.insert($scope.expense, {KeyProperty:"expense_code"});			
		}
//save function end
//fab button functions
		$scope.demo = {
	        topDirections: ['left', 'up'],
	        bottomDirections: ['down', 'right'],
	        isOpen: false,
	        availableModes: ['md-fling', 'md-scale'],
	        selectedMode: 'md-fling',
	        availableDirections: ['up', 'down', 'left', 'right'],
	        selectedDirection: 'up'
	      }; 
	      $scope.save = function(){
	      	$('#mySignup').click();
		}

		$scope.viewExpense = function(){
			 location.href ='#/home';
		}
//end of fab button functions

//final value function strat 

	$scope.finalamount = function(amount,tax){


		if (amount != "" && amount != null && tax != "" && tax != null ) {
			$scope.totalUSD = parseInt(amount) +parseInt(tax);
		}else if( tax == "" || tax == null){
			$scope.totalUSD = parseInt(amount);
		}
	}
//final value function end
//file uploader function start
		$scope.uploader = function(){
			$mdDialog.show({
                templateUrl: 'expenses_partial/expenses_dialog_partial.html',
                controller: EyeController                 
            })
		}

		 function EyeController($scope, $mdDialog, $rootScope,$state) {
            $scope.uploadimages = {val:[]};
            $scope.uploadimages.val = UploaderService.loadBasicArray(); 
            //directive table content start
			    $scope.$on('viewRecord', function(event, args) {
			    	$scope.uploadimages.val.splice(args,1);
				 
			  });			  
			     $scope.toggleSearch = false;   
				  $scope.headers = [
				    {
				      name: 'Name', 
				      field: 'name'
				    },{
				      name:'Size', 
				      field: 'size'
				    } 
				  ];
			  $scope.custom = {name: 'bold', size:'grey'};
			  $scope.sortable = ['name', 'size'];
			  $scope.thumbs = 'thumb';
			  $scope.count = 3;


//directive table conten end.

            $scope.closeDialog = function() {
                $mdDialog.hide();
            }
            $scope.AddCus = function() {
                 $scope.uploadimages.val = UploaderService.loadBasicArray();
             }
        }
	//file uploader function end
})//END OF AppCtrl
.controller('viewctrl', function ($scope,$mdToast,$state,$rootScope, $objectstore, $mdDialog, mfbDefaultValues, $window, $objectstore, $auth, $q, $http, $compile, $timeout) {
	
 $rootScope.prodSearch = "";
 $scope.self = this;
 $scope.self.searchText = "";

	$scope.testarr = [
				 {name:"Starred",id:"favouriteStarNo", src:"img/ic_grade_48px.svg",divider:true},
                 {name:"Date",id:"date", src:"img/ic_add_shopping_cart_48px.svg",divider:false},                         
                 {name:"Expense No",id:"Reference",  src:"img/ic_add_shopping_cart_48px.svg",divider:false},
                 {name:"Amount",id:"Amount",  src:"img/ic_add_shopping_cart_48px.svg",divider:false},                         
                 {name:"Supplier",id:"Assigncustomer",  src:"img/ic_add_shopping_cart_48px.svg",divider:false},
                 {name:"Category",id:"Category",  src:"img/ic_add_shopping_cart_48px.svg",divider:true},
                 {name:"Status 1",id:"Status1",  src:"img/ic_add_shopping_cart_48px.svg",divider:false},
                 {name:"Status 2",id:"Status2",  src:"img/ic_add_shopping_cart_48px.svg",divider:false},
                 {name:"Status 3",id:"Status3",  src:"img/ic_add_shopping_cart_48px.svg",divider:false}                   
                        
	]; 
       
       

  //calander function start

		$scope.caldisfun = function(obj){

			if (obj.Status == "Paid") {
				$scope.calanderdisable = true;

			}else if (obj.Status == "Unpaid") {
				$scope.calanderdisable = false;
			};

		}


  //calander function end 

//fab button functions
$scope.demo = {
	        topDirections: ['left', 'up'],
	        bottomDirections: ['down', 'right'],
	        isOpen: false,
	        availableModes: ['md-fling', 'md-scale'],
	        selectedMode: 'md-fling',
	        availableDirections: ['up', 'down', 'left', 'right'],
	        selectedDirection: 'up'
	      };
	   		$scope.addexpense = function(){
	   			 location.href ='#/Add_Expenses';
		} 
//end of fab button functions	
			$scope.expenses = [];
//load all expenses function start
			$scope.loadAllExpenses=function(){
				var client = $objectstore.getClient("expense12th");
				client.onGetMany(function(data){
					if (data){
						$scope.expenses = data;	
						console.log(data);	
						
					}
				});	
				client.onError(function(data){
					$mdDialog.show(
						$mdDialog.alert()
						.parent(angular.element(document.body))
						.title('This is embarracing')
						.content('There was an error retreving the data.')
						.ariaLabel('Alert Dialog Demo')
						.ok('OK')
						.targetEvent(data)
						);
				});
				client.getByFiltering("*");
			};
//load all expenses function end

//table click function start

$scope.tableclickfunc = function(obj){	 
	if($scope.tableclickArr!= null){
		for(i=0;i<obj.UploadImages.val.length;i++){
			if(obj.UploadImages.val[i].name=== $scope.tableclickArr.name)
				obj.UploadImages.val.splice(i, 1);
		}
	}
$scope.tableclickArr = null;
}
//table click function end 
$scope.tableclickArr= {};
 $scope.$on('viewRecord', function(event, args) {		 
			    $scope.tableclickArr=args;
});
//directive table content start
$scope.toggleSearch = false;   
				  $scope.headers = [
				    {
				      name: 'Name', 
				      field: 'name'
				    },{
				      name:'Size', 
				      field: 'size'
				    } 
				  ];
			  $scope.custom = {name: 'bold', size:'grey'};
			  $scope.sortable = ['name', 'size'];
			  $scope.thumbs = 'thumb';
			  $scope.count = 3;
//directive table conten end.

//favourite button function start
$scope.favouriteFunction = function(obj){
		var client = $objectstore.getClient("expense12th");		
		 	client.onComplete(function(data){ 

		 		if (obj.favouriteStar) {		 				 
		 		var toast = $mdToast.simple()
				  .content('Add To Favourite')
				  .action('OK')
				  .highlightAction(false)
				  .position("bottom right");
			$mdToast.show(toast).then(function() {
				//whatever
			});
		 		}else if (!(obj.favouriteStar)) {

		 		var toast = $mdToast.simple()
					  .content('Remove from Favourite')
					  .action('OK')
					  .highlightAction(false)
					  .position("bottom right");
				$mdToast.show(toast).then(function() {
					//whatever
				});
		 		};			
			});
			client.onError(function(data){				 
				var toast = $mdToast.simple()
					  .content('Error Occure while Adding to Favourite')
					  .action('OK')
					  .highlightAction(false)
					  .position("bottom right");
				$mdToast.show(toast).then(function() {
					//whatever
				});
			});	

		if (obj.favouriteStarNo == 1 ) {
			obj.favouriteStarNo = 0;
		}
		else if (obj.favouriteStarNo == 0){
			obj.favouriteStarNo = 1;
		};
		obj.favouriteStar = !obj.favouriteStar;		         
        client.insert(obj, {KeyProperty:"expense_code"});	
}
//favourite button function end

//full amount function start
$scope.fullamount = function(obj,event){
	if ((obj.Amount != "" && obj.Amount != null && obj.Tax != "" && obj.Tax != null) || (event.keyCode === 80)) {
		obj.totalUSD = parseInt(obj.Amount) + parseInt(obj.Tax);
	};
}
//full amount function end

$scope.updateexpenses = function(updatedForm){
	var client = $objectstore.getClient("expense12th"); 
	updatedForm.disableForm = false;
			client.onComplete(function(data){
				$mdDialog.show(
					$mdDialog.alert()
					.parent(angular.element(document.body))
			         //.title('This is embarracing')
			         .content('Expense Successfully Updated')
			         .ariaLabel('')
			         .ok('OK')
			         .targetEvent(data)			         
			         );
			});
			client.onError(function(data){
				$mdDialog.show(
					$mdDialog.alert()
					.parent(angular.element(document.body))
			         //.title('This is embarracing')
			         .content('Error Updating Expense')
			         .ariaLabel('')
			         .ok('OK')
			         .targetEvent(data)
			         );
			});			         
        client.insert(updatedForm, {KeyProperty:"expense_code"});
    }
 $scope.expensesDelete = function(deleteform) {
            var client = $objectstore.getClient("expense12th");
            client.onComplete(function(data) {
                $mdDialog.show(
                    $mdDialog.alert()
                    .parent(angular.element(document.body))
                    .content('Expense Successfully Deleted')
                    .ariaLabel('')
                    .ok('OK')
                    .targetEvent(data)
                );
                $state.go($state.current, {}, {
                    reload: true
                });
            });
            client.onError(function(data) {
                $mdDialog.show(
                    $mdDialog.alert()
                    .parent(angular.element(document.body))
                    .content('Error Deleting Expense')
                    .ariaLabel('')
                    .ok('OK')
                    .targetEvent(data)
                );
            });
            client.deleteSingle(deleteform.expense_code, "expense_code");
        }

	});


  