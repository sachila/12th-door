angular.module('mainApp', ['ngMaterial', 'directivelibrary','12thdirective','uiMicrokernel',
        'ngAnimate','ui.router'
    ])
    .config(function($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/home');
        $stateProvider.state('home', {
                url: '/home',
                controller: 'AppCtrlGet',
                templateUrl: 'payment_partial/payment_view.html'
            })
            .state('Add_Payment', {
                url: '/Add_Payment',
                controller: 'AppCtrlAdd',
                templateUrl: 'payment_partial/payment_add.html'
            })
    })
    .config(function($mdThemingProvider) {
        $mdThemingProvider.theme('datePickerTheme')
            .primaryPalette('teal');
    })
    //*****************ADD CONTROLLER********************************
    .controller('AppCtrlAdd', function($scope, $state, $objectstore, $location,
        $mdDialog, $window, $objectstore, $auth, $q,
        $http, $compile, $timeout, $mdToast,$rootScope) {
    
        $scope.project = {};
        $scope.addtasks = [];
        $scope.addstaffs = [];
        $scope.staffs = [];
        $scope.tasks = [];
        $scope.customerNames = [];
        $scope.contacts=[];
        $rootScope.results=[];
        $scope.checkAbility=true;
         //Autocomplete stuff
    $rootScope.self = this;
    $rootScope.self.tenants   = loadAll();
    $rootScope.selectedItem1  = null;
    $rootScope.self.searchText    = null;
    $rootScope.self.querySearch   = querySearch;
    
    
    $scope.tableclickArr= {};
 $scope.$on('viewRecord', function(event, args) {   
       $scope.tableclickArr=args;
});
//directive table content start
$scope.toggleSearch = false;   
    $scope.outstandingInvoices = [
       {
           date: 'sep 3,2015',         
           invno: 'INVO-0001',
           duedate:'sep 5,2015',
           amount:'100$',
           balance:'50$',
           pamount:'50$'
    },{
           date: 'sep 4,2015',         
           invno: 'INVO-0002',
           duedate:'sep 5,2015',
           amount:'100$',
           balance:'50$',
           pamount:'50$'
          
       },{
           date: 'sep 5,2015',         
           invno: 'INVO-0003',
           duedate:'sep 5,2015',
           amount:'100$',
           balance:'50$',
           pamount:'50$'
       },{
           date: 'sep 6,2015',         
           invno: 'INVO-0004',
           duedate:'sep 5,2015',
           amount:'100$',
           balance:'50$',
           pamount:'50$'
        },{
           date: 'sep 7,2015',         
           invno: 'INVO-0005',
           duedate:'sep 5,2015',
           amount:'100$',
           balance:'50$',
           pamount:'50$'
       },{         
           date: 'sep 8,2015',         
           invno: 'INVO-0006',
           duedate:'sep 5,2015',
           amount:'100$',
           balance:'50$',
           pamount:'50$'
      }         
         
     ];
    
     $scope.comment = function(ev) {
            $mdDialog.show({
               templateUrl: 'payment_partial/addComment.html',
               targetEvent: ev,
               controller:'AppCtrlAdd',
               locals: {
                  dating: ev
               }
            })
         }
      $scope.history = function(ev) {
            $mdDialog.show({
               templateUrl: 'payment_partial/paymentHistory.html',
               targetEvent: ev,
               controller:'AppCtrlAdd',
               locals: {
                  dating: ev
               }
            })
         }
     $scope.cancel = function() {
         $mdDialog.cancel();
      };
//     $scope.custom = {name: 'bold', size:'grey'};
//     $scope.sortable = ['date', 'invoiceno','duedate','amount','balance','paidamount'];
//     $scope.thumbs = 'thumb';
     
    // $scope.count = 3;
//directive table conten end
      $scope.checkInvo=false;
      $scope.loadInvoices=function(){
          
          $scope.checkInvo=true;
          
      }

    function querySearch (query) {
    
      $scope.enter = function(keyEvent) {
        if (keyEvent.which === 13)
        { 
          if($rootScope.selectedItem1 === null)
          {
            $rootScope.selectedItem1 = query;  
            console.log($rootScope.results);
          }else{
            console.log($rootScope.selectedItem1);
          }
        }
      }
      
      //Custom Filter
      $rootScope.results=[];
      for (i = 0, len = $scope.customerNames.length; i<len; ++i){
        if($scope.customerNames[i].display.indexOf(query) !=-1)
        {
          $rootScope.results.push($scope.customerNames[i]);
         
        } 
      }
        
        console.log($rootScope.results);
      return $rootScope.results;
    }
     
     $scope.customerNames = [];

     
    function loadAll() {

       var client = $objectstore.getClient("contact");
     client.onGetMany(function(data){
      if (data){ 
      // $scope.contact =data;
        for (i = 0, len = data.length; i<len; ++i){
          $scope.customerNames.push ({display:data[i].CustomerFname +' '+ data[i].CustomerLname});
        } 
          console.log($scope.customerNames);

      }
     }); 
     client.onError(function(data){
      $mdDialog.show(
       // $mdDialog.alert()
       //   .parent(angular.element(document.body))
       //   .title('Sorry')
       //   .content('There is no products available')
       //   .ariaLabel('Alert Dialog Demo')
       //   .ok('OK')
       //   .targetEvent(data)
      );
     });
     client.getByFiltering("*");
    }
           
        $scope.submit = function() {
            var client = $objectstore.getClient("payment");
            client.onComplete(function(data) {
                $mdDialog.show($mdDialog.alert()
                    .parent(angular
                        .element(document.body))
                    .content(
                        'Payment Added Successfully.')
                    .ariaLabel(
                        'Alert Dialog Demo')
                    .ok('OK')
                    .targetEvent(
                        data));
            });
            client.onError(function(data) {
                $mdDialog.show($mdDialog.alert()
                    .parent(angular
                        .element(document.body))
                    .content(
                        'There was an error saving the data.'
                    )
                    .ariaLabel('Alert Dialog Demo')
                    .ok(
                        'OK')
                    .targetEvent(data));
            });
            $scope.payment.favoritestar = false;
            $scope.payment.paymentid = "-999";
            $scope.payment.customer = $rootScope.selectedItem1.display;
            client.insert($scope.payment, {
                KeyProperty: "paymentid"
            })
        }
        $scope.addstaff = function() {
            $scope.addstaffs.push({
                sno: $scope.addstaffs.length + 1,
                staffname: "",
                shr: "",
                removebtnDisable: false
            })
        }
        $scope.removeStaff = function(index) {
            $scope.addstaffs.splice(index, 1);
        };
        $scope.addtask = function() {
            $scope.addtasks.push({
                tno: $scope.addtasks.length + 1,
                task: "",
                thr: "",
                removebtnDisable: false
            })
        }
        $scope.removeTask = function(index) {
            $scope.addtasks.splice(index, 1);
        };

        function init() {
            $scope.addstaff();
            $scope.addtask();
        }
        init();
        $scope.demo = {
            topDirections: ['left', 'up'],
            bottomDirections: ['down', 'right'],
            isOpen: false,
            availableModes: ['md-fling', 'md-scale'],
            selectedMode: 'md-fling',
            availableDirections: ['up', 'down', 'left', 'right'],
            selectedDirection: 'up'
        };
        $scope.addCustomer = function() {
            $('#add')
                .animate({
                    width: "100%",
                    height: "100%",
                    borderRadius: "0px",
                    right: "0px",
                    bottom: "0px",
                    opacity: 0.25
                }, 400, function() {
                    location.href = '#/Add_Payment';
                });
        }
        $scope.Customerview = function() {
           
                    location.href = '#/home';
        }
        $scope.savebtn = function() {
            $('#save')
                .animate({
                    width: "100%",
                    height: "100%",
                    borderRadius: "0px",
                    right: "0px",
                    bottom: "0px",
                    opacity: 0.25
                }, 400, function() {
                    $('#mySignup')
                        .click();
                });
        }
        $scope.save = function() {
          $timeout(function(){
            $('#mySignup')
                .click();
             })
        }
        $scope.viewpromotion = function() {
            $('#view')
                .animate({
                    width: "100%",
                    height: "100%",
                    borderRadius: "0px",
                    right: "0px",
                    bottom: "0px",
                    opacity: 0.25
                }, 400, function() {});
        }
         $scope.nAmount ="";
        $scope.creditAvailabe=function(type){
        if(type=="Invoice Settlement"){
          $scope.cAmount=20;
          return $scope.cAmount;
        }
        }
        $scope.netAmount = function(){
            if(($scope.payment.amountReceived.length !=0) && ($scope.payment.bankCharges.length !=0) ){
            $scope.nAmount =parseInt($scope.payment.amountReceived-$scope.payment.bankCharges);
            return $scope.nAmount;
            }
           // console.log($scope.nAmount);
        }
    }) //END OF AppCtrlAdd
    //***************RETRIVE CONTROLLER**********************
    .controller('AppCtrlGet', function($scope, $state,$rootScope, $objectstore, $location,
        $mdDialog, $window, $objectstore, $auth, $q,
        $http, $compile, $timeout, $mdToast,$log) {
        $scope.payments = [];
        $scope.checkAbilityBtn = true;
        $scope.checkAbilityEditing = true;
        $scope.proSearch = "";
        $scope.sortproNumber = "number";
        $scope.sortproDate = "date";
        $scope.sortbudgetedHours = "bhours";
        $scope.sorthourlyprojectRate = "prate";
        $scope.sortfinalprojectAmount = "pamount";
    
    
    // sort start
    
 $rootScope.prodSearch = "";
 $scope.self = this;
 $scope.self.searchText = "";

 $scope.testarr = [
                 {name:"Starred",id:"Starred", src:"img/ic_grade_48px.svg"},
                 {name:"Date",id:"date", src:"img/ic_add_shopping_cart_48px.svg"},                         
                 {name:"Payment Id",id:"paymentid",  src:"img/ic_add_shopping_cart_48px.svg"}
                              
                        
 ]; 
    
    
    // sort end 
    
    
    
    
        $scope.outstandingInvoices = [
       {
           date: 'sep 3,2015',         
           invno: 'INVO-0001',
           duedate:'sep 5,2015',
           amount:'100$',
           balance:'50$',
           pamount:'50$'
    },{
           date: 'sep 4,2015',         
           invno: 'INVO-0002',
           duedate:'sep 5,2015',
           amount:'100$',
           balance:'50$',
           pamount:'50$'
          
       },{
           date: 'sep 5,2015',         
           invno: 'INVO-0003',
           duedate:'sep 5,2015',
           amount:'100$',
           balance:'50$',
           pamount:'50$'
       },{
           date: 'sep 6,2015',         
           invno: 'INVO-0004',
           duedate:'sep 5,2015',
           amount:'100$',
           balance:'50$',
           pamount:'50$'
        },{
           date: 'sep 7,2015',         
           invno: 'INVO-0005',
           duedate:'sep 5,2015',
           amount:'100$',
           balance:'50$',
           pamount:'50$'
       },{         
           date: 'sep 8,2015',         
           invno: 'INVO-0006',
           duedate:'sep 5,2015',
           amount:'100$',
           balance:'50$',
           pamount:'50$'
      }         
         
     ];
    
        $scope.loadAllpayments = function() {
            var client = $objectstore.getClient("payment");
            client.onGetMany(function(data) {
                if (data) {
                    $scope.payments = data;
                }
            });
            client.onError(function(data) {
                $mdDialog.show($mdDialog.alert()
                    .parent(angular
                        .element(document.body))
                    .title(
                        'This is embarracing')
                    .content(
                        'There was an error retreving the data.'
                    )
                    .ariaLabel('Alert Dialog Demo')
                    .ok(
                        'OK')
                    .targetEvent(data));
            });
            client.getByFiltering("*");
        };
        $scope.updateProject = function(updatedform, pid) {
            var client = $objectstore.getClient("payment");
            client.onComplete(function(data) {
                $mdDialog.show($mdDialog.alert()
                    .parent(angular
                        .element(document.body))
                    .content(
                        'Payment details updated Successfully'
                    )
                    .ariaLabel('Alert Dialog Demo')
                    .ok(
                        'OK')
                    .targetEvent(data));
            });
            client.onError(function(data) {
                $mdDialog.show($mdDialog.alert()
                    .parent(angular
                        .element(document.body))
                    .title(
                        'This is embarracing')
                    .content(
                        'There was an error updating the project details.'
                    )
                    .ariaLabel('Alert Dialog Demo')
                    .ok(
                        'OK')
                    .targetEvent(data));
            });
            updatedform.paymentid = pid;
            client.insert(updatedform, {
                KeyProperty: "paymentid"
            });
        }
        $scope.deleteProject = function(deleteform, ev) {
            var confirm = $mdDialog.confirm()
                .parent(angular.element(
                    document.body))
                .title('')
                .content(
                    'Are You Sure You Want To Delete This Record?')
                .ok(
                    'Delete')
                .cancel('Cancel')
                .targetEvent(ev);
            $mdDialog.show(confirm)
                .then(function() {
                    var client = $objectstore.getClient("payment");
                    client.onComplete(function(data) {
                        $mdDialog.show($mdDialog.alert()
                            .parent(
                                angular.element(
                                    document.body))
                            .content(
                                'Record Successfully Deleted'
                            )
                            .ariaLabel('')
                            .ok('OK')
                            .targetEvent(
                                data));
                        $state.go($state.current, {}, {
                            reload: true
                        });
                    });
                    client.onError(function(data) {
                        $mdDialog.show($mdDialog.alert()
                            .parent(
                                angular.element(
                                    document.body))
                            .content(
                                'Error Deleting Record'
                            )
                            .ariaLabel('')
                            .ok('OK')
                            .targetEvent(
                                data));
                    });
                    client.deleteSingle(deleteform.paymentid,
                        "paymentid");
                }, function() {
                    $mdDialog.hide();
                });
        }
        $scope.favouriteFunction = function(obj) {
            var client = $objectstore.getClient("payment");
            client.onComplete(function(data) {
                if (obj.favoritestar) {
                    var toast = $mdToast.simple()
                        .content(
                            'Add To Favourite')
                        .action('OK')
                        .highlightAction(
                            false)
                        .position("bottom right");
                    $mdToast.show(toast)
                        .then(function() {});
                } else if (!(obj.favoritestar)) {
                    var toast = $mdToast.simple()
                        .content(
                            'Remove from Favourite')
                        .action(
                            'OK')
                        .highlightAction(false)
                        .position(
                            "bottom right");
                    $mdToast.show(toast)
                        .then(function() {});
                };
            });
            client.onError(function(data) {
                $mdDialog.show($mdDialog.alert()
                    .parent(angular
                        .element(document.body))
                    .content(
                        'Error Occure while Adding to Favourite'
                    )
                    .ariaLabel('')
                    .ok('OK')
                    .targetEvent(
                        data));
            });
            obj.favoritestar = !obj.favoritestar;
            client.insert(obj, {
                KeyProperty: "paymentid"
            });
        }
        $scope.sortFunciton = function(name) {
            $scope.projectSearch = name;
            self.searchText = null;
        }
        $scope.testfunc = function() {
            self.searchText = "true"
        }
        $scope.addStaffupdate = function(add) {
            add.push({
                sno: add.length + 1,
                staffname: "",
                shr: "",
                removebtnDisable:true
            })
        }
        $scope.removeStaffupdate = function(index, proj) {
            $scope.projects[index].staffs.splice(proj, 1);
        }
        $scope.addTaskupdate = function(add) {
            add.push({
                tno: add.length + 1,
                taskName: "",
                thr: "",
                removebtnDisable: true
            })
        }
        $scope.removeTaskupdate = function(index, proj) {
            $scope.projects[index].tasks.splice(proj, 1);
        }
        $scope.onChangeEditing = function(cbState, state) {
            if (cbState == true) {
                $scope.checkAbilityEditing = false;
                $scope.checkAbilityBtn = false;
            } else {
                $scope.checkAbilityEditing = true;
                $scope.checkAbilityBtn = true;
            }
            for (var i = state.length - 1; i >= 0; i--) {
                state[i].removebtnDisable = true;
            };
        }
        $scope.chekAblityRemovebtn = function(state) {
            for (var i = state.length - 1; i >= 0; i--) {
                state[i].removebtnDisable = true;
            };
        }
        $scope.demo = {
            topDirections: ['left', 'up'],
            bottomDirections: ['down', 'right'],
            isOpen: false,
            availableModes: ['md-fling', 'md-scale'],
            selectedMode: 'md-fling',
            availableDirections: ['up', 'down', 'left', 'right'],
            selectedDirection: 'up'
        };
        $scope.addCustomer = function() {
            $('#add')
                .animate({
                    width: "100%",
                    height: "100%",
                    borderRadius: "0px",
                    right: "0px",
                    bottom: "0px",
                    opacity: 0.25
                }, 400, function() {
                    location.href = '#/Add_Payment';
                });
        }
        $scope.Customerview = function() {
            $('#view')
                .animate({
                    width: "100%",
                    height: "100%",
                    borderRadius: "0px",
                    right: "0px",
                    bottom: "0px",
                    opacity: 0.25
                }, 400, function() {
                    location.href = '#/home';
                });
        }
        $scope.savebtn = function() {
            $('#save')
                .animate({
                    width: "100%",
                    height: "100%",
                    borderRadius: "0px",
                    right: "0px",
                    bottom: "0px",
                    opacity: 0.25
                }, 400, function() {
                    $('#mySignup')
                        .click();
                });
        }
        $scope.save = function() {
            $('#mySignup')
                .click();
        }
        $scope.viewpromotion = function() {
            $('#view')
                .animate({
                    width: "100%",
                    height: "100%",
                    borderRadius: "0px",
                    right: "0px",
                    bottom: "0px",
                    opacity: 0.25
                }, 400, function() {});
        }
    }) //End of ApCtrlGet

										
   
  .factory('InvoiceService', function($rootScope){
  $rootScope.testArray = {val: []};
  return {
  
  setArray: function(newVal) {
        $rootScope.testArray.val.push(newVal);

     return $rootScope.testArray;
    },
     removeArray: function(newVals) {

    $rootScope.testArray.val.splice(newVals, 1);
    return $rootScope.testArray;
   }

  }    
  
});